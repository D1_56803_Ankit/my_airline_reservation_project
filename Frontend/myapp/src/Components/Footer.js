import "./Footer.css";
import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';


const Footer=()=> {
    return (

    <div class="bottom" style={{marginTop:"0px"}}>
    <footer className="footer-distributed">
  <div className="footer-left">
  <h3>
      Book <span>My Flight</span>
    </h3>
  <a href="/" className="logo">
            <img src={require("./images/logo.jpg")} alt="CoolBrand"/>
            </a>
    <p className="footer-links">
      
      <a href="#">Book</a>
      <a href="/travelInfo">Travel Information</a>
      <a href="#">Special Offers</a>
      <a href="/covidInfo">Covid Info</a>
      <a href="/FAQs">FAQs</a>
    </p>
    <p className="footer-company-name">Book My Flight © 2022</p>
  </div>
  <div className="footer-center">
    <div>
      <i className="fa fa-map-marker" />
      <p>
        <span>Sunbeam Infotech</span> Hinjewadi, Pune
      </p>
    </div>
    <div>
      <i className="fa fa-phone" />
      <p>+91 555 555 5555</p>
    </div>
    <div>
      <i className="fa fa-envelope" />
      <p>
        <a href="mailto:bookmyflight4@gmail.com">bookmyflight4@gmail.com</a>
      </p>
      <div>
        <br></br>
      <img src={require("./images/footimg.png")} alt="CoolBrand"/>
      </div>

    </div>
  </div>
  <div className="footer-right">
    <p className="footer-company-about">
      <span>About the company</span>
      Book My Flight is proud to be one of the youngest Airline Reservation System 
      to serve, and thanks to our customers’ response to our offerings. 
    </p>
    <div className="footer-icons">
      <a href="#">
        <i className="fa fa-facebook" />
      </a>
      <a href="#">
        <i className="fa fa-twitter" />
      </a>
      <a href="#">
        <i className="fa fa-linkedin" />
      </a>
      <a href="#">
        <i className="fa fa-github" />
      </a>
    </div>
  </div>
</footer>

    
</div>
    );
  }
  export default Footer;