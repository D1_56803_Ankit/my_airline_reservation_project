import AdminHeader from "./AdminHeader"
import AfterLoginHeader from "./AfterLoginHeader"
import Header from "./Header"

const SelectHeader = () => {

    const {loginStatus, role} = sessionStorage
    if(loginStatus == 1 && role=="user")
    return <AfterLoginHeader/>
    else if(loginStatus == 1 && role=="admin")
    return <AdminHeader/>
    else
    return <Header/>
}

export default SelectHeader