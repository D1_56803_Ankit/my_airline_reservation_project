import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { URL } from '../config';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router';
const styles = {
    td: {
        paddingLeft: "20px",
        paddingRight: "20px",
        paddingBottom: "20px",
        width: '485px'
    },
    td1: {
        paddingLeft: "20px",
        fontWeight: 'bold',
        color: "black"
    },
    span: {
        fontWeight: 'bold',

    },
    td2: {
        paddingLeft: "20px",
        paddingRight: "20px",
        paddingBottom: "20px",
        width: '40.80%'
    },
    td3: {
        paddingLeft: "20px",
        fontWeight: 'bold',
        color: "black",
        width: '40%'
    },
    td4: {
        paddingLeft: "20px",
        paddingRight: "20px",
        paddingBottom: "20px",
        width: '39.65%'
    },
    td5: {
        paddingLeft: "20px",
        fontWeight: 'bold',
        color: "black",
        width: '39.65%'
    }
}

const Content3 = () => {
    const [destinationCity, setDestinationCity] = useState('')
    const [sourceCity, setSourceCity] = useState('')
    const [departureDate, setDepartureDate] = useState('')
    const [arrivalDate, setArrivalDate] = useState('')
    const [noOfPassenger, setNoOfPassenger] = useState(1)
    const [travelClass, setTravelClass] = useState('economy')
    const [flightid, setFlightid] = useState(0)
    const [date, setDate] = useState('')
    const [firstName, setFirstName] = useState("")
    const [bookingId, setBookingId] = useState(0)

    const navigate = useNavigate()

    const searchFlight = () => {
        if (sourceCity.length == 0) {
            toast.warning('please select source city')
        } else if (destinationCity.length == 0) {
            toast.warning('please select destination city')
        } if (departureDate.length == 0) {
            toast.warning('please select departure date')
        } else {

            // url to make signin api call
            const url = `${URL}/flights/searchflights/${sourceCity}/${destinationCity}/${departureDate}`

            // make api call using axios
            axios.get(url).then((response) => {
                // get the server result
                const result = response.data
                console.log(result)
                if (result['status'] == 'success') {
                    sessionStorage['searchFlightResult'] = JSON.stringify(response.data)
                    sessionStorage['travelClass'] = travelClass
                    console.log(result.data)
                    navigate('/flightdetails', { state: { result: result.data } })

                } else {
                    toast.error(result['error'])
                    console.log("no data found")
                }

                //navigate('/flightdetails', { state: { result: result } })
                // } else {
                //   toast.error('Invalid user name or password')
                // }

            })
        }
    }


    const myTrip = () => {
        if (bookingId == 0) {
            toast.warning("Enter Booking Id")
        } else if (firstName.length == 0) {
            toast.warning("Enter First Name")
        } else {
            const url = `${URL}/bookings/mytrip/${bookingId}/${firstName}`
            axios.get(url).then((response) => {
                console.log(response)
                const result = response.data
                console.log(result)
                console.log(result.data.id)
                if (result['status'] == 'success') {
                    //sessionStorage['flightId']=flightid
                    navigate('/ticket', { state: { bookingId: result.data.id } })

                } else {
                  
                    toast.error(result['error'])

                }

            })
        }
    }

    const checkIn = () => {
        if (bookingId == 0) {
            toast.warning("Enter Booking Id")
        } else if (firstName.length == 0) {
            toast.warning("Enter First Name")
        } else {
            const url = `${URL}/bookings/checkin/${bookingId}/${firstName}`
            axios.put(url).then((response) => {
                const result = response.data
                console.log(result)

                if (result['status'] == 'success') {
                    //sessionStorage['flightId']=flightid
                    navigate('/ticket', { state: { bookingId: result.data.Id } })
                    toast.success("Checked In Success")
                } else {
                    toast.error(result['error'])
                    console.log("no data found")
                }

            })
        }
    }

    const CheckFlight = () => {
        if (flightid == 0) {
            toast.warning('please enter flightId')
        } else if (date.length == 0) {
            toast.warning('please enter date')
        } else {

            console.log(flightid, date)
            // url to make signin api call
            const url = `${URL}/flights/${flightid}`

            // make api call using axios
            axios.get(url).then((response) => {
                // get the server result
                const result = response.data
                console.log(result)
                if (result['status'] == 'success') {
                    //sessionStorage['flightId']=flightid
                    navigate('/flightstatus', { state: { flightId: flightid, departdate: date } })
                } else {
                    toast.error(result['error'])
                    console.log("no data found")
                }

            })
        }
    }

    return (
        <div style={{ backgroundColor: '#E5E4E2' }}>
            <div class="row d-flex justify-content-center" >
                <div class="col-md-8" >
                    <div style={{ backgroundColor: "white", border: "solid", borderColor: "lightGray" }}>
                        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                            <li class="nav-item " role="presentation"> <button class="nav-link active" id="faq_tab_1-tab" data-bs-toggle="tab" data-bs-target="#faq_tab_1" type="button" role="tab" aria-controls="faq_tab_1" aria-selected="true">
                                <div class="d-flex flex-column lh-lg"> <span style={styles.span} > <i className=" fa fa-thin fa-plane"></i> Flight</span> </div>
                            </button> </li>
                            <li class="nav-item" role="presentation"> <button class="nav-link" id="faq_tab_2-tab" data-bs-toggle="tab" data-bs-target="#faq_tab_2" type="button" role="tab" aria-controls="faq_tab_2" aria-selected="false">
                                <div class="d-flex flex-column lh-lg">  <span style={styles.span}> <i class='fa fa-calendar'></i> My Trips</span> </div>
                            </button> </li>
                            <li class="nav-item" role="presentation"> <button class="nav-link" id="faq_tab_3-tab" data-bs-toggle="tab" data-bs-target="#faq_tab_3" type="button" role="tab" aria-controls="faq_tab_3" aria-selected="false">
                                <div class="d-flex flex-column lh-lg"> <span style={styles.span}> <i class="fa fa-check-circle"></i> Check-in</span> </div>
                            </button> </li>
                            <li class="nav-item" role="presentation"> <button class="nav-link" id="faq_tab_4-tab" data-bs-toggle="tab" data-bs-target="#faq_tab_4" type="button" role="tab" aria-controls="faq_tab_4" aria-selected="false">
                                <div class="d-flex flex-column lh-lg">  <span style={styles.span}><i class="fa fa-check-circle"></i> Flight Status</span> </div>
                            </button> </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade active show" id="faq_tab_1" role="tabpanel" aria-labelledby="faq_tab_1-tab">
                                <div class="container p-3">
                                    <div >
                                        <table>
                                            <tr style={{ color: "white" }} >
                                                <td style={styles.td1}>
                                                    <label for="flightfrom" >From</label> </td>
                                                <td style={styles.td1}>
                                                    <label for="flightto">To</label></td>
                                            </tr>
                                            <tr>
                                                <td style={styles.td}>
                                                    <input type="text" class="form-control" id="flightfrom"
                                                        onChange={(e) => { setSourceCity(e.target.value) }} /></td>
                                                <td style={styles.td}> <input type="text" class="form-control" id="flightto"
                                                    onChange={(e) => { setDestinationCity(e.target.value) }} /></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <table>
                                            <tr style={{ color: "white" }} >
                                                <td style={styles.td1} >
                                                    <label for="departdate" >Departure Date</label> </td>
                                                <td style={styles.td1}>
                                                    <label for="arrivedate">Arrival Date</label></td>
                                            </tr>
                                            <tr>
                                                <td style={styles.td}>
                                                    <input type="date" class="form-control" id='departdate' placeholder="Depart"
                                                        onChange={(e) => { setDepartureDate(e.target.value) }} />
                                                </td>
                                                <td style={styles.td}>
                                                    <input type="date" class="form-control" id='arrivedate' placeholder="Return"
                                                        onChange={(e) => { setArrivalDate(e.target.value) }} /></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <table>
                                            <tr style={{ color: "white" }} >
                                                <td style={styles.td1}>
                                                    <label for="passenger" >No of Passenger</label> </td>
                                                <td style={styles.td1}>
                                                    <label for="travel">Travel Class</label></td>
                                            </tr>
                                            <tr>
                                                <td style={styles.td}>
                                                    <input type="number" defaultValue={1} class="form-control" placeholder="No of Passenger" id='passenger'
                                                        onChange={(e) => { setNoOfPassenger(e.target.value) }} />
                                                </td>
                                                <td style={styles.td}>
                                                    <select itemID='travel' class="form-select form-control" id="inputGroupSelect02" placeholder="Travel Class"
                                                        onChange={(e) => { setTravelClass(e.target.value) }} >
                                                        <option value="economy">Economy</option>
                                                        <option value="business">Business</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="mt-4 d-flex justify-content-end"> <button onClick={searchFlight} class="btn btn-primary custom-button px-5" >Show Flights</button> </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="faq_tab_2" role="tabpanel" aria-labelledby="faq_tab_2-tab">
                                <div class="container p-3 scroll-y">
                                    <div >
                                        <table>
                                            <tr style={{ color: "white" }} >
                                                <td style={styles.td3}>
                                                    <label for="flightfrom" >Booking Id</label> </td>
                                                <td style={styles.td3}>
                                                    <label for="flightto">First Name</label></td>
                                                <td style={styles.td3}></td>
                                            </tr>
                                            <tr>
                                                <td style={styles.td2}>
                                                    <input type="text" class="form-control" id="bookingId" placeholder="Enter Booking Id"
                                                        onChange={(e) => { setBookingId(e.target.value) }}
                                                    /></td>
                                                <td style={styles.td2}> <input type="text" class="form-control" id="firstName" placeholder="Enter First Name"
                                                    onChange={(e) => { setFirstName(e.target.value) }}
                                                /></td>
                                                <td style={styles.td2}><button class="btn btn-primary " onClick={myTrip}>Retrive Booking</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="faq_tab_3" role="tabpanel" aria-labelledby="faq_tab_3-tab">
                                <div class="container p-3 mt-4">
                                    <div >
                                        <table>
                                            <tr style={{ color: "white" }} >
                                                <td style={styles.td3}>
                                                    <label for="flightfrom" >Booking Id</label> </td>
                                                <td style={styles.td3}>
                                                    <label for="flightto">First Name</label></td>
                                                <td style={styles.td3}></td>
                                            </tr>
                                            <tr>
                                                <td style={styles.td2}>
                                                    <input type="text" class="form-control" id="bookingId" placeholder="Enter Booking Id"
                                                        onChange={(e) => { setBookingId(e.target.value) }}
                                                    /></td>
                                                <td style={styles.td2}> <input type="text" class="form-control" id="firstName" placeholder="Enter First Name"
                                                    onChange={(e) => { setFirstName(e.target.value) }}
                                                /></td>
                                                <td style={styles.td2}><button class="btn btn-primary" onClick={checkIn}>Check In Flight</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="faq_tab_4" role="tabpanel" aria-labelledby="faq_tab_4-tab">
                                <div class="container p-3">
                                    <div >
                                        <table>
                                            <tr style={{ color: "white" }} >
                                                <td style={styles.td5}>
                                                    <label for="flightId" >Flight Id</label> </td>
                                                <td style={styles.td5}>
                                                    <label for="date">Date</label></td>
                                                <td style={styles.td5}></td>
                                            </tr>
                                            <tr>
                                                <td style={styles.td4}>
                                                    <input type="text" class="form-control" id="flightId" placeholder="Enter Flight Id"
                                                        onChange={(e) => { setFlightid(e.target.value) }} /></td>
                                                <td style={styles.td4}> <input type="date" class="form-control" id="date"
                                                    onChange={(e) => { setDate(e.target.value) }} /></td>
                                                <td style={styles.td4}>
                                                    <button class="btn btn-primary " onClick={CheckFlight}>Check Flight Status</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>






    )
}

export default Content3
