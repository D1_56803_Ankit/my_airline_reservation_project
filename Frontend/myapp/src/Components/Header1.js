
import{Link} from 'react-router-dom'
//import Disclaimer from './../pages/disclaimer';
 const Header1 = () => {
    
    return (
        <header>
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark navbar ">
          <div className="container-fluid topcolor">
            <div >
            <Link className="nav-link title topheader" to="/home"><h2 className="logo"> <img className="logo" width="160px" height="50px" src="https://pbs.twimg.com/profile_images/816741301/logo.jpg" alt="logo" /></h2></Link> 
           
            </div>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            
            <div className="collapse navbar-collapse" id="navbarNav">
          
              <ul className="navbar-nav navbar-nav me-auto mb-5 mb-lg-0">
              <li  >
                  <Link  className="nav-link title topheader" to="#"></Link>
                </li>
                <li  >
                  <Link  className="nav-link title topheader" to="#"></Link>
                  </li>
                <li className="nav-item">
                  <Link className="nav-link title topheader " to="/home"> Home</Link>
                </li>
                <li  >
                  <Link  className="nav-link title topheader" to="/privacypolicy">Privacy Policy</Link>
                  </li>
                <li >
                <Link className="nav-link title topheader" to="/terms">Regulations</Link>       
                </li>
                <li  >
                  <Link  className="nav-link title topheader" to="/disclaimer">Disclaimer</Link>
                </li>
                <li >
                <Link  className="nav-link title topheader" to="#"></Link>
              </li>
            
                  <Link className="nav-link title topheader"  to="/users/signin"> <button className="btn btn-outline-success  buttsignin " type="submit">Sign in</button></Link>
               
              
                  <Link className="nav-link title topheader"  to="/signup"> <button className=" btn btn-outline-success  buttsignup " type="submit">Sign up</button></Link>
               
              </ul>
             
        
            </div>
          </div>
        </nav>
      </header>
      
              
    )
}
export default Header1