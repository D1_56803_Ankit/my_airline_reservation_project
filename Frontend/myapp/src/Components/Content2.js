import React from "react";
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./Header";

const styles = {
  div: {
    marginTop: "0px",
    width: "100%",
    height: "450px",
    mode: "pad",
  },
};

class Content2 extends React.Component {
  render() {
    return (
      <div>
        
        <div className="row">
            <div className="col-12">
              <Carousel>
                <Carousel.Item interval={1500}>
                  <img
                    //src={require('C:/Users/Mayur/Desktop/d1.png')}
                    //src={require('C:/Users/Mayur/Desktop/c11.jpg')}
                    src={require("./images/c1.jpg")}
                    style={styles.div}
                    alt="First slide"
                  />
                  {
                    <Carousel.Caption>
                      {/* <h3>First slide label</h3>
                      <p>
                        Nulla vitae elit libero, a pharetra augue mollis
                        interdum.
                      </p> */}
                    </Carousel.Caption>
                  }
                </Carousel.Item>

                <Carousel.Item interval={1500}>
                  <img
                    src={require("./images/c2.jpg")}
                    //src={require('C:/Users/Mayur/Desktop/c11.jpg')}
                    
                    style={styles.div}
                    alt="Second slide"
                  />
                  <Carousel.Caption>
                    {/* <h3>Second slide label</h3>
                    <p>
                      Nulla vitae elit libero, a pharetra augue mollis interdum.
                    </p> */}
                  </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>
                  <img
                    src={require("./images/c3.jpg")}
                    style={styles.div}
                    alt="Third slide"
                  />
                  <Carousel.Caption>
                    {/* <h3>Third slide label</h3>
                    <p>
                      Nulla vitae elit libero, a pharetra augue mollis interdum.
                    </p> */}
                  </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>
                  <img
                    src={require("./images/c4.jpg")}
                    //src={require('C:/Users/Mayur/Desktop/d3.jpg')}
                    style={styles.div}
                    alt="Fourth slide"
                  />
                  <Carousel.Caption>
                    {/* <h3>Fourth slide label</h3>
                    <p>
                      Nulla vitae elit libero, a pharetra augue mollis interdum.
                    </p> */}
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    src={require("./images/c5.jpg")}
                    //src={require('C:/Users/Mayur/Desktop/d4.jpg')}
                    style={styles.div}
                    alt="Fifth slide"
                  />
                  <Carousel.Caption>
                    {/* <h3>Fifth slide label</h3>
                    <p>
                      Nulla vitae elit libero, a pharetra augue mollis interdum.
                    </p> */}
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    src={require("./images/c6.jpg")}
                    style={styles.div}
                    alt="Sixth slide"
                  />
                  <Carousel.Caption>
                    {/* <h3>Sixth slide label</h3>
                    <p>
                      Nulla vitae elit libero, a pharetra augue mollis interdum.
                    </p> */}
                  </Carousel.Caption>
                </Carousel.Item>
              </Carousel>
            </div>
          </div>
        
      </div>
    );
  }
}

export default Content2;
