import { useLocation } from "react-router"
import { useNavigate } from "react-router"
import { useState } from "react"
import SelectHeader from "../Components/SelectHeader"
import { toast } from "react-toastify"
import Content4 from "../Components/Content4"
import Footer from "../Components/Footer"



const FlightDetails=()=>{
    const navigate = useNavigate()
    const {state} = useLocation()
    const { result } = state
    //const { result }=sessionStorage.getItem['searchFlightResult']
    const[travelClass, setTravelClass]=useState(sessionStorage.getItem('travelClass'))
    //var result=JSON.parse(sessionStorage.getItem("searchFlightResult"))
    const[selectedFlightId, setSelectedFlightId]=useState()
    const[selectedFlightFare, setSelectedFlightFare]=useState()
    const[selectedFlightOtherFare, setSelectedFlightOtherFare]=useState()
    const[selectedScheduleId, setSelectedScheduleId]=useState("")
    console.log(result)
    sessionStorage['flightId'] = selectedFlightId
    sessionStorage['fare'] = selectedFlightFare
    sessionStorage['otherFare'] = selectedFlightOtherFare
    sessionStorage['travelClass'] = travelClass
    sessionStorage['scheduleId'] = selectedScheduleId 
    
    return(
        <div style={{width:'100%', height:'100%',backgroundColor:'#E5E4E2'}}>
            <SelectHeader/>
        <div className="container" style={{width:'100%', height:'400px',backgroundColor:'#E5E4E2'}}>

        <h1>Flight List</h1>
        <div >

        <table className="table"  >
            <thead>

                <tr>
                   <th>Id</th>
                   <th>Airline Name</th>
                   <th>Departure Date</th>
                   <th>Duration</th>
                   <th>Arrival Date</th>
                   <th>Business Class</th> 
                   <th>Economy Class</th> 
                </tr>

            </thead>

            <tbody>
                {
                    result.map((flight)=>(
                        <tr key={flight.flight_id}>
                            <td>{flight.flight_id}</td>
                            <td>{flight.airline_name}</td>
                            <td>{flight.departure_date}</td>
                            <td>{flight.duration}</td>
                            <td>{flight.arrival_date}</td> 
                           <td>
                            <input type="radio" id="businessClass" name="classfare" value={flight.business_class_fare} 
                            onChange={(e)=>{setSelectedFlightFare(e.target.value); setSelectedFlightId(flight.flight_id);
                            setTravelClass('business'); setSelectedFlightOtherFare(flight.economy_class_fare); setSelectedScheduleId(flight.schedule_id)}}></input>
                               Rs. {flight.business_class_fare}
                            </td>
                            <td>
                            <input type="radio" id="arrivalClass" name="classfare" value={flight.economy_class_fare} 
                            onChange={(e)=>{setSelectedFlightFare(e.target.value); setSelectedFlightId(flight.flight_id);
                            setTravelClass('economy'); setSelectedFlightOtherFare(flight.business_class_fare); setSelectedScheduleId(flight.schedule_id)}}></input>
                            Rs. {flight.economy_class_fare}
                            </td>
                            
                        </tr>
                    )
                    )
                }
            </tbody>
        </table>
        <button
            onClick={()=>{navigate('/')}}
            className="btn btn-primary">Back</button>
        <button
            onClick={()=>{
            if(sessionStorage['loginStatus']==1)
                navigate('/bookingdetails', {state:{classType:travelClass}})
            else 
            {
                toast.error('Login first to continue booking..')
                navigate('/login')
            }}}
            className="btn btn-primary float-end">Continue</button>
        </div>
        </div>
        <Content4/>
        <Footer/>
        </div>
    )

}
export default FlightDetails
