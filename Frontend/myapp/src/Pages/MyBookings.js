import { useEffect, useState } from "react"
import { URL } from "../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useNavigate } from "react-router"
import SelectHeader from "../Components/SelectHeader"
import Footer from "../Components/Footer"
import Content4 from '../Components/Content4'

const MyBookings = () => {
    const [ MyBookings, setMyBookings] = useState([])
    const [bookingId , setBookingId] = useState(0)
    const navigate = useNavigate()
    
    useEffect(ViewMyBookings,[])

function ViewMyBookings(){

    const url=`${URL}/bookings/mybookings/${sessionStorage.getItem('id')}`
    axios.get(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success' && result['data'] !=0){
            setMyBookings(result['data'])
                toast.info('Showing Your bookings..')       
        }else{
            toast.warning('You do not have any bookings')
            navigate('/') 
        }

    })
}

    return (
        <div style={{backgroundColor:'#E5E4E2'}}>
            <SelectHeader/>
            <div>
          <br/>          
            <table className="table table-bordered table-hover table-warning">
                <thead>
                    <tr style={{color:"#5C0632"}}>
                        <th>Booking ID</th>
                        <th>Booking Date</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Flight ID</th>
                        <th>Source city</th>
                        <th>Destination city</th>
                        <th>Departure airport name</th>
                        <th>Arrival airport name</th>
                        <th>Departure Date </th>
                        <th>Departure Time </th>
                        <th>Arrival Date </th>
                        <th>Arrival Time </th>
                        <th>Seat No</th>
                        <th>Total fare</th>
                        <th>Booking Status</th>
                        <th>Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                    {
                        MyBookings.map( (mb) => (
                            <tr key={mb.booking_id}>
                                <td>{mb.booking_id}</td> 
                                <td>{mb.booking_date }</td> 
                                <td>{mb.first_name }</td> 
                                <td>{mb.last_name }</td>
                                <td>{mb.flight_id  }</td>
                                <td>{mb.source_city }</td>
                                <td>{mb.destination_city }</td>
                                <td>{mb.departure_airport_name }</td>
                                <td>{mb.arrival_airport_name  }</td>
                                <td>{mb.departure_date }</td>
                                <td>{mb.departure_time }</td>
                                <td>{mb.arrival_date }</td>
                                <td>{mb.arrival_time}</td>
                                <td>{mb.seat_no }</td>
                                <td> Rs. {mb.total_fare}</td>
                                <td style={{fontWeight:"bold"}}>{ mb.ticket_status}
                                </td>
                                <td>
                                    <button className="btn btn-danger"
                                   onClick={()=>{navigate('/cancelbooking',
                                   {state:{bookingid :mb.booking_id, bookingStatus: mb.ticket_status}})}} >Cancel </button>
                                </td>
                            </tr>
                        )    
                        )
                    }
                </tbody>
            </table>
            </div>
            <Content4/>
            <Footer/>
        </div>
    )
}

export default MyBookings