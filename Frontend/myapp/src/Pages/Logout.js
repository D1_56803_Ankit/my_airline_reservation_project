import { useEffect } from "react"
import { useNavigate } from "react-router"
import Home from "./Home"

const Logout = () => {
  const navigate = useNavigate()

  const removeUser = () =>{
     sessionStorage.removeItem('firstName')
     sessionStorage.removeItem('lastName')
     sessionStorage.removeItem('id')
     sessionStorage.removeItem('loginStatus')

     navigate('/login')
    
  }
  useEffect(removeUser(),[])
return (
  <div>
    <h2>Thank you for using our service.....!!!!</h2>
  </div>
)

}

export default Logout