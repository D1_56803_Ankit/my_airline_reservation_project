import axios from "axios"
import { useState } from "react"
import { useNavigate } from "react-router"
import { toast } from "react-toastify"
import Footer from "../Components/Footer"
import SelectHeader from "../Components/SelectHeader"
import { URL } from "../config"

const ResetPassword = () => {
    const { id } = sessionStorage
    const [currentPass, setCurrenPass] = useState('')
    const [newPass, setNewPass] = useState('')
    const [verifyPass, setVerifyPass] = useState('')
    
    const navigate = useNavigate()

function resetPass(){
if(currentPass.length == 0){
    toast.warning('Password can not be empty')
}else if(newPass.length == 0){
    toast.warning('Password can not be empty')
}else if(verifyPass.length == 0){
    toast.warning('Password can not be empty')
}else if(newPass!=verifyPass){
    toast.error('password does not match')
}else{

  const url = `${URL}/users/v4/resetpass/${id}/${currentPass}/${newPass}`
    axios.patch(url).then( (response) => {
        const result = response.data
        console.log(result)
        if(result['status'] == 'success')
        {
            toast.success('Password changed successfully')
            navigate('/login')
        }else{
        toast.error(result['error'])
        }
    })
}
}

    return (
    <div style={{"width":'100%',"height":'100%',backgroundColor:'#E5E4E2'}}>
          <SelectHeader/>
        <div >
       
        <div  style={{marginLeft:'420px',width:'500px', height:'545px',backgroundColor:'#E5E4E2'}}>
        <h1 style={{marginTop:'80px'}}>Set up new password</h1>
          <div className="form" style={{marginTop:'25px'}}>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Current password
              </label>
              <input
                onChange={(e) => {
                  setCurrenPass(e.target.value)
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                New Password
              </label>
              <input
                onChange={(e) => {
                  setNewPass(e.target.value)
                }}
                type="password"
                className="form-control"
              />
            </div> 

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Verify Password
              </label>
              <input
                onChange={(e) => {
                  setVerifyPass(e.target.value)
                }}
                type="password"
                className="form-control"
              />
            </div>

            <div>
              <button onClick={resetPass} className="btn btn-primary" style={{marginTop:'20px', width:'500px',backgroundColor:'#5C0632'}}>
                Reset Password
              </button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
      <Footer/>
    </div> 
    )
}

export default ResetPassword