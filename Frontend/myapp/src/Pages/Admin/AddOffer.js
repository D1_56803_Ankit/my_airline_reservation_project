import { useState } from "react"
import { URL } from "../../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useNavigate } from "react-router"
import SelectHeader from "../../Components/SelectHeader"
import Content4 from "../../Components/Content4"
import Footer from "../../Components/Footer"

const AddOffer = () => {

  const navigate = useNavigate()
  const [promocode, setPromocode] = useState('')
  const [discount, setDiscount] = useState(0)
  const [min_txn_amount, setMintxamt] = useState(0)
  const [validon, setValidon] = useState('Offer valid for all Banks Debit and Credit cards')


  function AddNewOffer() {

    if (promocode.length == 0) {
      toast.warning('Please enter promocode')
    } else if (discount <= 0) {
      toast.warning('Please enter discount')
    } else if (min_txn_amount <= 0) {
      toast.warning('Please enter min_txn_amount')
    } else {
      const body = {
        promocode,
        discount,
        min_txn_amount,
        validon,
      }

      // url to call the api
      const url = `${URL}/offers/add`

      // http method: post
      // body: contains the data to be sent to the API
      axios.post(url, body).then((response) => {
        // get the data from the response
        const result = response.data
        console.log(result)
        if (result['status'] == 'success') {
          toast.success('Successfully added new offer')

          // navigate to the signin page
          navigate('/editoffers')
        } else {
          toast.error(result['error'])
        }
      })
    }
  }

  return (
    <div style={{ backgroundColor: '#E5E4E2' }}>
      <SelectHeader />
      <div >
        <br />
        <br />
        <h3 style={{ backgroundColor: "#5C0632", color: "white" }}>Add offer here</h3>
        <hr />
        <div className="form-group row" style={{ marginLeft: "50px" }}>
          <label htmlFor="promocode" className="col-sm-2 col-form-label" style={{ fontWeight: "bold" }}>Promocode</label>
          <div className="col-sm-10">
            <input type="text" className="form-control" id="promocode"
              onChange={(e) => {
                setPromocode(e.target.value)
              }}
              placeholder="Enter promocode" style={{ width: "500px" }} />
          </div>
        </div>
        <div className="form-group row" style={{ marginLeft: "50px" }}>
          <label htmlFor="discount" className="col-sm-2 col-form-label" style={{ fontWeight: "bold" }}>Offer Discount</label>
          <div className="col-sm-10">
            <input type="number" className="form-control" id="discount"
              onChange={(e) => {
                setDiscount(e.target.value)
              }}
              placeholder="Enter Offer Discount in Rs." style={{ width: "500px" }} />
          </div>
        </div>
        <div className="form-group row" style={{ marginLeft: "50px" }}>
          <label htmlFor="txnamt" className="col-sm-2 col-form-label" style={{ fontWeight: "bold" }}>Minimum transaction amount</label>
          <div className="col-sm-10">
            <input type="number" className="form-control" id="txnamt"
              onChange={(e) => {
                setMintxamt(e.target.value)
              }}
              placeholder="Min. transaction amount in Rs." style={{ width: "500px" }} />
          </div>
        </div>
        <div className="form-group row" style={{ marginLeft: "50px" }}>
          <label htmlFor="validon" className="col-sm-2 col-form-label" style={{ fontWeight: "bold" }}>Valid on</label>
          <div className="col-sm-10">
            <input type="text" className="form-control" id="validon" disabled
              onChange={(e) => {
                setValidon(e.target.value)
              }}
              placeholder="Offer valid for all Banks' Debit and Credit cards" style={{ width: "500px" }} />
          </div>
        </div>
        <br />
        <div className="form-group row" style={{ marginLeft: "450px" }}>
          <div >
            <button type="submit" className="btn btn-warning btn-lg" onClick={AddNewOffer}>Add Offer</button>
          </div>
        </div>

      </div>
      <Content4 />
      <Footer />
    </div>
  )
}

export default AddOffer