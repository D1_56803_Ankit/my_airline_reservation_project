import { useLocation, useNavigate } from "react-router";
import CurrencyInput from 'react-currency-input-field'
import SelectHeader from "../../Components/SelectHeader";
import { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { URL } from "../../config";
import Content4 from "../../Components/Content4";
import Footer from "../../Components/Footer";

const styles = {
  td: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "485px",
  },
  td1: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
  },
  span: {
    fontWeight: "bold",
  },
  td2: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "40.80%",
  },
  td3: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "40%",
  },
  td4: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "39.65%",
  },
  td5: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "39.65%",
  },
  div: {
    marginLeft: "140px",
  },
};
const AddSchedule = () => {
  const navigate = useNavigate()
  const { state } = useLocation()
  const { flight } = state
  const [flight_id, setFlight_id] = useState(flight.id)
  const [departure_date, setDeparture_date] = useState('')
  const [arrival_date, setArrival_date] = useState('')
  const [departure_time, setDeparture_time] = useState('')
  const [arrival_time, setArrival_time] = useState('')
  const [flight_status, setFlight_status] = useState('')


  const AddNewSchedule = () => {
  if (departure_date.length == 0) {
      toast.warning('Please enter departure date')
    } else if (departure_time.length == 0) {
      toast.warning('Please enter departure time')
    } else if (arrival_date.length == 0) {
      toast.warning('Please enter arrival date')
    } else if (arrival_time.length == 0) {
      toast.warning('Please enter arrival time')
    } else {
      console.log(flight_id)
      const body = {
        flight_id,
        departure_date,
        arrival_date,
        departure_time,
        arrival_time,
        flight_status,
      }

      // url to call the api
      const url = `${URL}/schedules/`

      // http method: post
      // body: contains the data to be sent to the API
      axios.post(url, body).then((response) => {
        // get the data from the response
        const result = response.data
        console.log(result)
        if (result['status'] == 'success') {
          toast.success('Successfully added new schedule')
          // navigate to the signin page
          navigate('/editflight')
        } else {
          toast.error(result['error'])
        }
      })

    }

  }

  return (
    <div style={{ backgroundColor: '#E5E4E2', height: '100%' }}>
      <SelectHeader />
      <div className="col mt-4 d-flex justify-content-center"><h1>Add Flight Details</h1></div>
      <div
        className="tab-pane fade active show"
        id="faq_tab_1"
        role="tabpanel"
        aria-labelledby="faq_tab_1-tab"
      >
        <div className="container p-3">
          <div style={styles.div}>
            <table>
              <tbody>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Airline name </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Departure airport name </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.airline_name}
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.departure_airport_name}
                      type="text"
                      readOnly
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Arrival airport name</label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Source city </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.arrival_airport_name}
                      type="text"
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.source_city}
                      type="text"
                      readOnly
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Destination city </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Business class fare </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.destination_city}
                      type="text"
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.business_class_fare}
                      type="number"
                      readOnly
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Economy class fare </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Business class capacity </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.economy_class_fare}
                      type="number"
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.business_class_capacity}
                      type="number"
                      readOnly
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Economy class capacity </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Flight total capacity </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.economy_class_capacity}
                      type="number"
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.flight_total_capacity}
                      readOnly
                      type="number"
                      className="form-control"
                    />

                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="row">

          </div>
        </div>
        <div
          className="tab-pane fade active show"
          id="faq_tab_1"
          role="tabpanel"
          aria-labelledby="faq_tab_1-tab"
        >
          <div className="container p-3">
            <div style={styles.div}>
              <table>
                <tbody>
                  <tr style={{ color: "white" }}>
                    <td style={styles.td1}>
                      <label htmlFor="flightid">Selected flightID </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="departdate">Departure date </label>
                    </td>
                  </tr>
                  <tr>
                    <td style={styles.td}>
                    <input value={flight.id} 
             
                      type="number"
                      readOnly
                      className="form-control"
                    />
                    </td>
                    <td style={styles.td}>
                      <input onChange={(e) => {
                        setDeparture_date(e.target.value)
                      }}
                      id="departdate"
                        type="date"
                        className="form-control"
                      />
                    </td>
                  </tr>
                  <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                      <label htmlFor="deptime">Departure time </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="arrivaldate">Arrival date</label>
                    </td>
                  </tr>
                  <tr>
                  <td style={styles.td}>
                      <input onChange={(e) => {
                        setDeparture_time(e.target.value)
                      }}
                        type="text"
                        id="deptime"
                        placeholder="hh:mm:ss"
                        className="form-control"
                      />
                      <span>Please enter in "HH:MM:SS" format</span>
                    </td>
                    <td style={styles.td}>
                      <input onChange={(e) => {
                        setArrival_date(e.target.value)
                      }}
                        type="date"
                        id="arrivaldate"
                        className="form-control"
                      />
                    </td>
                  </tr>
                  <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                      <label htmlFor="arrivaltime">Arrival time </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="fstatus">Flight status </label>
                    </td>
                  </tr>
                  <tr>
                  <td style={styles.td}>
                      <input onChange={(e) => {
                        setArrival_time(e.target.value)
                      }}
                        type="text"
                        id="arrivaltime"
                        placeholder="hh:mm:ss"
                        className="form-control"
                      />
                      <span>Please enter in "HH:MM:SS" format</span>
                    </td>
                    <td style={styles.td}>
                      <input onChange={(e) => {
                        setFlight_status(e.target.value)
                      }}
                        type="text"
                        value="scheduled"
                        id="fstatus"
                        className="form-control"
                        readOnly
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="row">

              <div className="col mt-4 d-flex justify-content-end">
                <button className="btn btn-primary custom-button px-5 justify-content-end"
                  style={{ backgroundColor: '#5C0632' }}
                  onClick={AddNewSchedule}
                >
                  Add Schedule
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Content4/>
      <Footer/>
    </div>
      );
};

      export default AddSchedule;
