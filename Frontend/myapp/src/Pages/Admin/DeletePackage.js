import axios from "axios"
import { useEffect } from "react"
import { useLocation, useNavigate } from "react-router"
import { toast } from "react-toastify"
import { URL } from "../../config"

const DeletePackage = () =>{

    const { state } = useLocation()
    const { packageid } = state
    const navigate = useNavigate()
    console.log(packageid)

    useEffect( () =>{
                    DeleteSelectedPackage()
                    } ,[] )

function DeleteSelectedPackage(){

    const url=`${URL}/packages/${packageid}`
    axios.delete(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success'){
                toast.success('Package deleted successfully') 
                navigate('/editpackage')       
        }else{
            toast.error(result['error'])
            
        }

    })
}

return (
    <div></div>
        )
}
export default DeletePackage