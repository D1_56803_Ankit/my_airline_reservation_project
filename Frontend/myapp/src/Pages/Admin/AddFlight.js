import { useLocation, useNavigate } from "react-router";
import CurrencyInput from 'react-currency-input-field'
import SelectHeader from "../../Components/SelectHeader";
import { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { URL } from "../../config";
import Content4 from "../../Components/Content4";
import Footer from "../../Components/Footer";

const styles = {
  td: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "485px",
  },
  td1: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
  },
  span: {
    fontWeight: "bold",
  },
  td2: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "40.80%",
  },
  td3: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "40%",
  },
  td4: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "39.65%",
  },
  td5: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "39.65%",
  },
  div: {
    marginLeft: "140px",
  },
};
const AddFlight = () => {
  const navigate = useNavigate()

  const [airline_name, setAirline_name] = useState('')
  const [departure_airport_name, setDeparture_airport_name] = useState('')
  const [arrival_airport_name, setarrival_airport_name] = useState('')
  const [source_city, setSource_city] = useState('')
  const [destination_city, setDestination_city] = useState('')
  const [business_class_fare, setBusiness_class_fare] = useState(0.0)
  const [economy_class_fare, setEconomy_class_fare] = useState(0.0)
  const [business_class_capacity, setBusiness_class_capacity] = useState(0)
  const [economy_class_capacity, setEconomy_class_capacity] = useState(0)
  const [flight_total_capacity, setFlight_total_capacity] = useState(0)

  const AddNewFlight = () => {
    if (airline_name.length == 0) {
      toast.warning('Please enter Airline name')
    } else if (departure_airport_name.length == 0) {
      toast.warning('Please enter Departure airport name')
    } else if (arrival_airport_name.length == 0) {
      toast.warning('Please enter Arrival airport name')
    } else if (source_city.length == 0) {
      toast.warning('Please enter Source city')
    } else if (destination_city.length == 0) {
      toast.warning('Please enter Destination city')
    } else if (business_class_fare == 0) {
      toast.warning('Please enter Business class fare')
    } else if (economy_class_fare == 0) {
      toast.warning('Please enter Economy class fare')
    } else if (business_class_capacity == 0) {
      toast.warning('Please enter Business class capacity')
    } else if (economy_class_capacity == 0) {
      toast.warning('Please enter Economy class capacity')
    } else if (flight_total_capacity == 0) {
      toast.warning('Please enter Flight total capacity')
    } else {
      const body = {
        airline_name,
        departure_airport_name,
        arrival_airport_name,
        source_city,
        destination_city,
        business_class_fare,
        economy_class_fare,
        business_class_capacity,
        economy_class_capacity,
        flight_total_capacity,
      }

      // url to call the api
      const url = `${URL}/flights/add`

      // http method: post
      // body: contains the data to be sent to the API
      axios.post(url, body).then((response) => {
        // get the data from the response
        const result = response.data
        console.log(result)
        if (result['status'] == 'success') {
          toast.success('Successfully added new flight')
          // navigate to the signin page
          navigate('/adminhome')
        } else {
          toast.error(result['error'])
        }
      })

    }

  }

  return (
    <div style={{ backgroundColor: '#E5E4E2', height: '100%' }}>
      <SelectHeader />
      <div className="col mt-4 d-flex justify-content-center"><h1>Add Flight Details</h1></div>

      <div
        className="tab-pane fade active show"
        id="faq_tab_1"
        role="tabpanel"
        aria-labelledby="faq_tab_1-tab"
      >
        <div className="container p-3">
          <div style={styles.div}>
            <table>
              <tbody>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Airline name </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Departure airport name </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setAirline_name(e.target.value)
                    }}
                      type="text"
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setDeparture_airport_name(e.target.value)
                    }}
                      type="text"
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Arrival airport name</label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Source city </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setarrival_airport_name(e.target.value)
                    }}
                      type="text"
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setSource_city(e.target.value)
                    }}
                      type="text"
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Destination city </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Business class fare </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setDestination_city(e.target.value)
                    }}
                      type="text"
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setBusiness_class_fare(e.target.value)
                    }}
                      type="number"
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Economy class fare </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Business class capacity </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setEconomy_class_fare(e.target.value)
                    }}
                      type="number"
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setBusiness_class_capacity(e.target.value)
                    }}
                      type="number"
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Economy class capacity </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Flight total capacity </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setEconomy_class_capacity(e.target.value)
                    }}
                      type="number"
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input onChange={(e) => {
                      setFlight_total_capacity(e.target.value)
                    }}
                      type="number"
                      className="form-control"
                    />

                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="row">

            <div className="col mt-4 d-flex justify-content-end">
              <button className="btn btn-primary custom-button px-5 justify-content-end"
                style={{ backgroundColor: '#5C0632' }}
                onClick={AddNewFlight}
              >
                Add Flight
              </button>
            </div>
          </div>
        </div>
      </div>
      <Content4 />
      <Footer />

    </div>
  );
};

export default AddFlight;
