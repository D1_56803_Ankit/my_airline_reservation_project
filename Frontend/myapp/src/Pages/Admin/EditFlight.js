import { useEffect, useState } from "react"
import { URL } from "../../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useNavigate } from "react-router"
import SelectHeader from "../../Components/SelectHeader"
import Footer from "../../Components/Footer"

const EditFlight = () => {
    const [ flights, setFlights] = useState([])
    const navigate = useNavigate()
    
    useEffect(ViewFlights,[])

function ViewFlights(){

    const url=`${URL}/flights/`
    axios.get(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success' && result['data'] !=0){
            setFlights(result['data'])
                toast.info('Showing available flights')       
        }else{
            toast.warning('No flights available now')
            navigate('/adminhome') 
        }

    })
}

    return (
        <div style={{backgroundColor:'#E5E4E2'}}>
            <SelectHeader/>
            <div>
          <br/>
            <button className="btn btn-warning btn-lg float-end" onClick={()=>{navigate('/addflight')}}>Click here to add new flight</button>
           
            <table className="table table-bordered table-hover table-warning">
                <thead>
                    <tr>
                        <th>Flight ID</th>
                        <th>Airline name</th>
                        <th>Departure airport name</th>
                        <th>Arrival airport name</th>
                        <th>Source city</th>
                        <th>Destination city</th>
                        <th>Business class fare</th>
                        <th>Economy class fare</th>
                        <th>Business class capacity</th>
                        <th>Economy class capacity</th>
                        <th>Flight total capacity</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        flights.map( (f) => (
                            <tr key={f.id}>
                                <td>{f.id}</td> 
                               <td>{f.airline_name }</td> 
                               <td>{f.departure_airport_name }</td>
                                <td>{f.arrival_airport_name }</td>
                                <td>{f.source_city }</td>
                                <td>{f.destination_city }</td>
                                <td>{f.business_class_fare}</td>
                                <td>{f.economy_class_fare }</td>
                                <td>{f.business_class_capacity}</td>
                                <td>{f.economy_class_capacity}</td>
                                <td>{f.flight_total_capacity}</td>
                                <td>
                                    <button className="btn btn-outline-danger btn-sm"
                                   onClick={()=>{navigate('/deleteflight',{state:{flightid :f.id}})}} >Delete Flight</button>
                                   <button className="btn btn-outline-success btn-sm"
                                   onClick={()=>{navigate('/addschedule',{state:{flight :f}})}} >Add Schedule</button>
                                    <button className="btn btn-outline-warning btn-sm"
                                   onClick={()=>{navigate('/viewschedule',{state:{flight :f}})}} >View Schedule</button>
                                </td>
                            </tr>
                        )    
                        )
                    }
                </tbody>
            </table>
            </div>
            <Footer/>
        </div>
    )
}

export default EditFlight