import Content2 from "../Components/Content2"
import Content3 from "../Components/Content3"
import AfterLoginHeader from "../Components/AfterLoginHeader"
import Content4 from "../Components/Content4"
import Footer from "../Components/Footer"

const LoginHome = () =>{
    const currentLoggedInUser = sessionStorage['firstName']
    return (
    <div style={{height: "1500px", backgroundRepeat : "no-repeat"}}>
        <AfterLoginHeader />
        <Content2 />
        <Content3 />
        <Content4/>
        <Footer/>
    </div>
    )
}
export default LoginHome