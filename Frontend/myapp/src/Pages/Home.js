import React from "react";
import Content2 from "../Components/Content2";
import BootstrapCarouselComponent from "../Components/Content2";
import Content3 from "../Components/Content3";
import Content4 from "../Components/Content4";
import Footer from "../Components/Footer";
import Header from "../Components/Header";

const Home = () => {
 
    return (
    
    <div style={{
            height: "1500px", 
            backgroundRepeat: "no-repeat"
                }}>
     <Header/> 
     <Content2/>
    <Content3 />
    <Content4/>
     <Footer/> 
    </div>
  );
};
export default Home;
