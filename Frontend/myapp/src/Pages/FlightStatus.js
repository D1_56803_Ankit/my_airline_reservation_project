import { useLocation, useNavigate } from "react-router";
import SelectHeader from "../Components/SelectHeader";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { URL } from "../config";
import Content4 from "../Components/Content4";
import Footer from "../Components/Footer";

const styles = {
  td: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "485px",
  },
  td1: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
  },
  span: {
    fontWeight: "bold",
  },
  td2: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "40.80%",
  },
  td3: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "40%",
  },
  td4: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "39.65%",
  },
  td5: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "39.65%",
  },
  div: {
    marginLeft: "140px",
  },
};
const FlightStatus = () => {
  const navigate = useNavigate()
  const { state } = useLocation()
  const { flightId, departdate } = state
   const [result, setResult] = useState([]) 
  const ViewFlightStatus = () => {

      // url to call the api
      const url = `${URL}/flights/checkStatus/${flightId}/${departdate}`

      // http method: post
      // body: contains the data to be sent to the API
      axios.get(url).then((response) => {
        // get the data from the response
        setResult(response.data.data)
        const detail=response.data.data
        console.log(detail)
        if (detail['status'] == 'success') {
          toast.success('Checking flight status')
          // navigate to the signin page
        } else {
          toast.error(detail['error'])
        }
      })

  }

  useEffect(ViewFlightStatus,[])

  return (
    <div style={{ backgroundColor: '#E5E4E2', height: '100%' }}>
      <SelectHeader />
      <div className="col mt-4 d-flex justify-content-center" style={{color:"#5C0632"}}><h1>Check Flight status</h1></div>
     <hr/>
      <div
        className="tab-pane fade active show"
        id="faq_tab_1"
        role="tabpanel"
        aria-labelledby="faq_tab_1-tab"
      >
          {
              result.map( (f) => ( 
          <div className="container p-3">
            <div style={styles.div}>
              <table>
                <tbody>
                  <tr style={{ color: "white" }}>
                    <td style={styles.td1}>
                      <label htmlFor="flightid">Flight ID </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="airline">Airline name</label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="from">From</label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="to">To</label>
                    </td>
                  </tr>
                  <tr>
                    <td style={styles.td}>
                    <input 
                        id="flightid" value={f.flight_id}
                      readOnly
                      className="form-control"
                    />
                    </td>
                    <td style={styles.td}>
                      <input 
                      id="airline" value={f.airline_name}
                      readOnly
                        className="form-control"
                      />
                    </td>
                    <td style={styles.td}>
                    <input 
                     id="from" value={f.source_city }
                      readOnly
                      className="form-control"
                    />
                    </td>
                    <td style={styles.td}>
                    <input 
                      id="to" value={f.destination_city }
                      readOnly
                      className="form-control"
                    />
                    </td>
                  </tr>
                  <tr style={{ color: "white" }}>
                    <td style={styles.td1}>
                      <label htmlFor="departdate">Departure date </label>
                    </td>
                  <td style={styles.td1}>
                      <label htmlFor="deptime">Departure time </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="arrivaldate">Arrival date</label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="arrivaltime">Arrival time </label>
                    </td>
                  </tr>
                  <tr>
                  <td style={styles.td}>
                      <input 
                        value={f.departure_date }
                        id="departdate"
                        readOnly
                        className="form-control"
                      />
                    </td>
                    <td style={styles.td}>
                      <input 
                        readOnly
                        value={f.departure_time}
                        id="deptime"
                        className="form-control"
                      />
                    </td>
                    <td style={styles.td}>
                    <input 
                     id="arrivaldate" value={f.arrival_date}
                      readOnly
                      className="form-control"
                    />
                    </td>
                    <td style={styles.td}>
                    <input 
                      id="arrivaltime" value={f.arrival_time}
                      readOnly
                      className="form-control"
                    />
                    </td>
                  </tr>
                  <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                      <label htmlFor="duration">Journey time </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="fstatus" style={{color:"#5C0632"}}>Flight status </label>
                    </td>
                  </tr>
                  <tr>
                  <td style={styles.td}>
                      <input 
                        value={f.duration}
                        id="duration"
                        readOnly
                        className="form-control"
                      />
                    </td>
                    <td style={styles.td}>
                    <input 
                        value={f.flight_status}
                        id="fstatus"
                        readOnly
                        className="form-control"
                        style={{backgroundColor:"green", color:"white"}}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
              <hr/>
            </div>

          </div>
              )
            )
        }
        </div>
        <Content4/>
        <Footer/>
      </div>
      );
};

      export default FlightStatus;
