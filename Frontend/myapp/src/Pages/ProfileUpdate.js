import axios from "axios"
import { useState } from "react"
import { useNavigate } from "react-router"
import { toast } from "react-toastify"
import AfterLoginHeader from "../Components/AfterLoginHeader"
import Content4 from "../Components/Content4"
import Footer from "../Components/Footer"
import SelectHeader from "../Components/SelectHeader"
import { URL } from "../config"

const ProfileUpdate = () => {

    const [ name_on_card,setNameOnCard ] = useState('') 
    const [ card_no, setCardNo ] = useState(0)
    const [ validity_date, setValidityDate ] = useState('')
    const [ cvv, setCvv ] = useState(0)
    const [ user_id, setUserId ] = useState(sessionStorage.getItem('id'))
    const [ first_name, setFirstName ] = useState('')
    const [ last_name, setLastName ] = useState('')

    const navigate = useNavigate()

    const updateProfile = () => {
        if(name_on_card.length == 0){
        toast.warning('Please enter Name On Card')
        }else if (card_no.length != 12){
            toast.warning('Please enter 12 digit card number')
        }else if (validity_date.length == 0){
            toast.warning('Please enter validity date')
        }else if (cvv.length != 3){
            toast.warning('Please enter 3 digit cvv')
        }else {
            const body = {
                name_on_card,
                card_no,
                validity_date,
                cvv,
                user_id,    
            }

            const url = `${URL}/paymentcards/add`

            axios.post(url, body).then((response) => {
                const result = response.data
                console.log(result)
                if (result['status'] == 'success'){
                    toast.success('Added your card')

                    navigate('/profileUpdate')
                }else {
                    toast.error(result['error'])
                }
            })
        }

    }

    const saveUser = () => {
        if(first_name.length == 0){
            toast.warning('Please enter first Name')
            }else if (last_name.length == 0){
                toast.warning('Please enter last name')
            }else {
                const body = {
                    first_name,
                    last_name,
                }
    
                const url = `${URL}/users/v4/${user_id}`
    
                axios.put(url, body).then((response) => {
                    const result = response.data
                    console.log(result)
                    if (result['status'] == 'success'){
                        toast.success('Name changed succesfully')
                        sessionStorage['firstName']= first_name
                        sessionStorage['lastName']= last_name
                        navigate('/loginhome')
                    }else {
                        toast.error(result['error'])
                    }
                })
            }
    }

    return (
        <div style={{"width":'100%',"height":'100%',backgroundColor:'#E5E4E2'}}>
           <SelectHeader/>

        <div >
            
            <div className="row">
                     
            <div className="col">
                
            </div>
                    <div className="col">
                    <h3 className="title">Add Payment Card</h3>
                        <div className="form">
                            <div className="mb-3">
                             <label className="label-control">
                                 Name on card
                             </label>   
                             <input 
                             onChange={ (e) => {
                                 setNameOnCard(e.target.value)
                             }}
                             type="text"
                             className="form-control"
                             ></input>
                            </div>
                            
                            <div className="mb-3">
                             <label className="label-control">
                             Card number
                             </label>
                             <input
                             onChange={ (e) => {
                                 setCardNo(e.target.value)
                             }}
                             type="number"
                             className="form-control"
                             ></input>
                            </div>

                            <div className="mb-3">
                             <label className="label-control">
                             Valid till
                             </label>
                             <input
                             onChange={ (e) => {
                                 setValidityDate(e.target.value)
                             }}
                             type="date"
                             className="form-control"
                             ></input>
                            </div>

                            <div className="mb-3">
                             <label className="label-control">
                             CVV
                             </label>
                             <input
                             onChange={ (e) => {
                                 setCvv(e.target.value)
                             }}
                             type="number"
                             className="form-control"
                             ></input>
                            </div>
                            
                            <div className="mb-3">
                             <button onClick={updateProfile} className="btn btn-primary">Update</button>   
                            </div>
                        </div>
                    </div>
                <div className="col"></div>
                <div className="col">
                <h3 className="title">Name Change</h3>
                            <div className="mb-3">
                             <label className="label-control">
                             First Name
                             </label>
                             <input
                             onChange={ (e) => {
                                 setFirstName(e.target.value)
                             }}
                             type="text"
                             placeholder={sessionStorage.getItem('firstName')}
                             className="form-control"
                             ></input>
                            </div>

                            <div className="mb-3">
                             <label className="label-control">
                             Last Name
                             </label>
                             <input
                             onChange={ (e) => {
                                 setLastName(e.target.value)
                             }}
                             type="text"
                             placeholder={sessionStorage.getItem('lastName')}
                             className="form-control"
                             ></input>
                            </div>
                            
                            <div className="mb-3">
                             <button onClick={saveUser} className="btn btn-primary">Save</button>   
                            </div>
                </div>
                <div className="col"></div>
            </div>
        </div>
        <Content4/>
        <Footer/>
        </div>
    )
}

export default ProfileUpdate