import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import Content4 from "../Components/Content4";
import Footer from "../Components/Footer";
import SelectHeader from "../Components/SelectHeader";
import { URL } from "../config";

const SelectPackage = () => {
  const navigate = useNavigate()
  const [selectedPackageId, setSelectedPackageId] = useState("");
  const [selectedPackageFare, setSelectedPackageFare] = useState("");
  const [selectedPackageName, setSelectedPackageName] = useState("");
  const { originalTravelClass } = sessionStorage.getItem('travelClass')
  const [travelClass, setTravelClass] = useState(sessionStorage.getItem('travelClass'))//economy//business
  const [selectedFlightFare, setSelectedFlightFare] = useState(sessionStorage.getItem('fare'))//1000
  const [selectedFlightOtherFare, setSelectedFlightOtherFare] = useState(sessionStorage.getItem('otherFare'))//1500
  const [extraBaggageAllowed, setExtraBaggageAllowed] = useState("")

  const [seatType, setSeatType] = useState("")//business//economy
  sessionStorage['selectedPackageId'] = selectedPackageId
  sessionStorage['selectedPackageFare'] = selectedPackageFare
  //   const refresh=()=>{
  //   if(seatType!=travelClass){   
  //     setTempFare(selectedFlightFare) //1000//1500
  //     setTravelClass(seatType)//business//economy
  //     setSelectedFlightFare(sessionStorage.getItem('otherFare'))//1500//1000
  //     setSelectedFlightOtherFare(tempFare)//1000//1500
  //     sessionStorage['travelClass']=travelClass//business//economy
  //   sessionStorage['fare']=selectedFlightFare//1500//1000
  //   sessionStorage['otherFare']=selectedFlightOtherFare//1000//1500
  //   }
  //   navigate('/user/selectPackage')
  // }
  sessionStorage['travelClass'] = travelClass//business//economy
  sessionStorage['fare'] = selectedFlightFare//1500//1000
  sessionStorage['otherFare'] = selectedFlightOtherFare//1000//1500
  sessionStorage['offerId'] = 10
  sessionStorage['offerDiscount'] = 100
  sessionStorage['extraBaggageAllowed'] = extraBaggageAllowed

  console.log(selectedPackageFare)
  console.log(selectedPackageId)
  const [packages, setPackages] = useState([]);
  //   console.log(result.data)
  //   useEffect(() => {
  const getPackages = () => {
    const url = `${URL}/packages/`
    axios.get(url).then((response) => {
      const result = response.data
      console.log(result)
      if (result['status'] == 'success') {
        setPackages(result.data)
      } else
        toast.error("No packages available");
    })

    const url2=`${URL}/offers/`
    axios.get(url2).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success'){
            setOffers(result['data'])
                toast.success('Checkout Special offers')       
        }else{
            toast.warning('No offer available now')
            navigate('/loginhome') 
        }

    })
  }
  useEffect(getPackages, []);
  console.log(packages)
  //var result=JSON.parse(sessionStorage.getItem("packages"))
  //console.log(result)

  const Continue = () => {
    if (selectedPackageId.length == 0) {
      toast.warning("Please select package")
    } else {
      navigate('/paymentdetails',
        { state: { seatType: seatType, selectedPackageFare: selectedPackageFare, selectedPackageName: selectedPackageName, package:packages } })
    }
  }

  return (
    <div>
      <SelectHeader />
      <div class="col mt-4 d-flex justify-content-center">
        <h1>Package page</h1>
      </div>
      <table className="table" >
        <thead>
          <tr>
            <th>Package Id</th>
            <th>Package Name</th>
            <th>Seat Type</th>
            <th>Food</th>
            <th>Beverages</th>
            <th>Baggage</th>
            <th>Package Fare</th>
          </tr>
        </thead>

        <tbody>
          {

            packages.map((p) => (
              <tr key={p.id}>
                <td>{p.id}</td>
                <td>{p.package_name}</td>
                <td>{p.seat_type}</td>
                <td>{p.food}</td>
                <td>{p.beverages}</td>
                <td>{p.baggage}</td>
                <td>
                  <input
                    type="radio"
                    id="package fare"
                    name="packagefare"
                    value={p.package_fare}
                    onChange={(e) => {
                      setSelectedPackageFare(e.target.value);
                      setSelectedPackageName(p.package_name);
                      setSelectedPackageId(p.id);
                      setSeatType(p.seat_type);
                      setExtraBaggageAllowed(p.baggage)
                      if (p.seat_type != travelClass) {
                        setTravelClass(p.seat_type)
                        setSelectedFlightFare(sessionStorage.getItem('otherFare'))
                        setSelectedFlightOtherFare(sessionStorage.getItem('fare'))
                      }
                    }}
                  ></input>
                  Rs. {p.package_fare}
                </td>
              </tr>

            ))

          }

        </tbody>
      </table>
          {/*
      <div class="col d-grid gap-2 d-md-flex" style={{ marginLeft: '1150px' }}>

        <input

          type="radio"
          id="Nopackage"
          name="Nopackagefare"
          value="0"
          onChange={(e) => {
            setSelectedPackageFare(e.target.value);
            setSelectedPackageName("skipped");
            setSelectedPackageId(0);
            setExtraBaggageAllowed(0);            
            setTravelClass({originalTravelClass})
          }} />
        <label htmlFor="Nopackage" >Skip package</label>
      </div> */}
      <br />
      <div class='row'>
        <div class="col d-grid gap-2 d-md-flex" style={{ marginLeft: '150px' }}>
          <button
            onClick={() => { navigate('/bookingdetails') }}
            className="btn btn-primary" style={{ backgroundColor: '#5C0632' }}>Back</button>
        </div> 


        <div class=" col d-grid gap-2 d-md-flex justify-content-md-end" style={{ marginRight: '150px' }}>
          <button
            onClick={Continue}
            className="btn btn-primary " style={{ backgroundColor: '#5C0632' }}>Continue</button>
        </div>
      </div>
      {/* <div class=" col d-grid gap-2 d-md-flex justify-content-md-center" style={{marginRight:'150px'}}>
        <button
            onClick={refresh}
            className="btn btn-primary " style={{backgroundColor:'#5C0632'}}>Refresh</button>
</div> */}
      <Content4 />
      <Footer />
    </div>

  );
};

export default SelectPackage;
