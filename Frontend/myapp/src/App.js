import './App.css';
import{BrowserRouter,Routes,Route,Link} from 'react-router-dom'
import Home from './Pages/Home';
import Content2 from './Components/Content2';
import Login from './Pages/Login';
import LoginHome from './Pages/LoginHome';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import FlightDetails from './Pages/FlightDetails';
import BookingDetails from './Pages/BookingDetails';
import SignUp from './Pages/SignUp' 
import ProfileUpdate from './Pages/ProfileUpdate';
import Offer from './Pages/Offers';
import ForgotPassword from './Pages/ForgotPassword';
import ResetPassword from './Pages/ResetPass';
import VerifyOtp from './Pages/OtpVerification';
import EditOffer from './Pages/Admin/EditOffer';
import DeleteOffer from './Pages/Admin/DeleteOffer';
import AddOffer from './Pages/Admin/AddOffer';
import AdminHome from './Pages/Admin/AdminHomePage';
import AddFlight from './Pages/Admin/AddFlight';
import AddPackage from './Pages/Admin/AddPackage';
import EditPackage from './Pages/Admin/EditPackage';
import DeletePackage from './Pages/Admin/DeletePackage';
import EditFlight from './Pages/Admin/EditFlight';
import DeleteFlight from './Pages/Admin/DeleteFlight';
import AddSchedule from './Pages/Admin/AddSchedule';
import Schedule from './Pages/Admin/ViewSchedule';
import EditSchedule from './Pages/Admin/EditSchedule';
import SelectPackage from './Pages/SelectPackage';
import PaymentDetails from './Pages/PaymentDetails';
import TicketDetails from './Pages/TicketDetails';
import FlightStatus from './Pages/FlightStatus';
import Bookings from './Pages/Admin/Bookings';
import MyBookings from './Pages/MyBookings';
import CancelBooking from './Pages/cancelBooking';
import CovidInfo from './Pages/CovidInfo';
import TravelInfo from './Pages/TravelInfo'
import FAQs from './Pages/FAQs';
import Footer from './Components/Footer';
import Payment from './Pages/Payment';

function App() {

 const LoggedInUser = () => {
    const loginStatus = sessionStorage['loginStatus']
    const role = sessionStorage['role']
    if(loginStatus=='1' && role == 'user')
    return <LoginHome />
    else if(loginStatus=='1' && role == 'admin')
    return <AdminHome/>
    else
    return <Home />
  }

  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route path='/' element={<LoggedInUser/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/home' element={<LoginHome/>}/>
        <Route path='/adminhome' element={<AdminHome/>}/>
 	      <Route path='/signup' element={<SignUp/>}/>
 	      <Route path='/loginhome' element={<LoginHome/>}/>
        <Route path='/flightdetails' element={<FlightDetails/>}/>
        <Route path='/bookingdetails' element={<BookingDetails/>}/>
        <Route path='/profileUpdate' element={<ProfileUpdate/>}></Route>
        <Route path='/offers' element={<Offer/>}></Route>
        <Route path='/editoffer' element={<EditOffer/>}></Route>
        <Route path='/forgotpassword' element={<ForgotPassword/>}></Route>
        <Route path='/resetpass' element={<ResetPassword/>}></Route>
        <Route path='/otpverify' element={<VerifyOtp/>}></Route>
        <Route path='/deleteoffer' element={<DeleteOffer/>}></Route>
        <Route path='/addoffer' element={<AddOffer/>}></Route>
        <Route path='/addflight' element={<AddFlight/>}></Route>
        <Route path='/addpackage' element={<AddPackage/>}></Route>
        <Route path='/editpackage' element={<EditPackage/>}></Route>
        <Route path='/deletepackage' element={<DeletePackage/>}></Route>
        <Route path='/editflight' element={<EditFlight/>}></Route>
        <Route path='/deleteflight' element={<DeleteFlight/>}></Route>
        <Route path='/addschedule' element={<AddSchedule/>}></Route>
        <Route path='/viewschedule' element={<Schedule/>}></Route>
        <Route path='/editschedule' element={<EditSchedule/>}></Route>
        <Route path='/selectpackage' element={<SelectPackage/>}></Route>
        <Route path='/paymentdetails' element={<PaymentDetails/>}></Route>
        <Route path='/ticket' element={<TicketDetails/>}></Route>
        <Route path='/flightstatus' element={<FlightStatus/>}></Route>
        <Route path='/bookings' element={<Bookings/>}></Route>
        <Route path='/mybookings' element={<MyBookings/>}></Route>
        <Route path='/cancelbooking' element={<CancelBooking/>}></Route>
        <Route path='/payment' element={<Payment/>}></Route>
        <Route path='/covidinfo' element={<CovidInfo/>}></Route>
        <Route path='/travelinfo' element={<TravelInfo/>}></Route>
        <Route path='/faqs' element={<FAQs/>}></Route>
        <Route path='/footer' element={<Footer/>}></Route>
      </Routes>
      </BrowserRouter>
      <ToastContainer theme="colored" />
    </div>
  );
}

export default App;
