package com.sunbeam;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.UserDao;
import com.sunbeam.entities.User;

@SpringBootTest
class UserDaoTest {
@Autowired
private UserDao userDao;
	
@Test	
void findById() {
	User result=userDao.findById(2);
	System.out.println("found"+result);
	
}

@Test
void findByEmail() {
	User user=userDao.findByEmail("ankit@gmail.com");
	System.out.println("found"+user);
	}

@Rollback(value = false)
@Transactional
@Test
void save() {
	User user=new User(0,"Akshay","Kirange","akshay@gmail.com","2222","user");
	userDao.save(user);
}

@Test	
void findAll() {
List<User> list=userDao.findAll();
	list.forEach(System.out::println);
}
} 
