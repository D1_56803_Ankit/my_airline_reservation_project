package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sunbeam.entities.Passenger;
import java.lang.String;
import java.util.List;

public interface PassengerDao extends JpaRepository<Passenger, Integer>{
Passenger findById(int id);

//@Modifying
//@Query(value="INSERT into passenger(first_name, last_name, email, password) VALUES(?1,?2,?3,?4)", nativeQuery = true)
//int addUser(String first_name, String last_name, String email, String password);

@Query(value = "select * from passenger where first_name=?1 and last_name=?2 and email=?3", nativeQuery = true)
Passenger findByFirstNameAndLastNameAndEmail(String first_name, String last_name, String email);

}
