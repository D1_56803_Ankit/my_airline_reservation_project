package com.sunbeam.daos;

import java.sql.Time;
import java.util.Date;

public interface FlightScheduleDtoProjection {

	Integer getFlight_id();
	Integer getSchedule_id();
	String getAirline_name();
	String getSource_city();
	String getDestination_city();
	Date getDeparture_date();
	Date getArrival_date();
	String getDuration();
	Double getEconomy_class_fare();
	Double getBusiness_class_fare();
	Time getDeparture_time();
	Time getArrival_time();
	String getFlight_status();
	}

