package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.Package;

public interface PackageDao extends JpaRepository<Package, Integer> {

	Package findById(int id);

}
