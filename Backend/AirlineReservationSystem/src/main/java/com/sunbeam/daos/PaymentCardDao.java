package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sunbeam.entities.PaymentCardDetails;

public interface PaymentCardDao extends JpaRepository<PaymentCardDetails, Integer> {

	PaymentCardDetails findById(int id);
	
	@Query(value = "select * from payment_card_details where user_id=?1", nativeQuery = true)
	List<PaymentCardDetails> findPaymentCardByUserId(int userIid);
}
