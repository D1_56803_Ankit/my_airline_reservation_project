package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sunbeam.entities.User;

public interface UserDao extends JpaRepository<User, Integer> {
User findById(int id);
User findByEmail(String email);

@Modifying
@Query(value="INSERT into user(first_name, last_name, email, password) VALUES(?1,?2,?3,?4)", nativeQuery = true)
int addUser(String first_name, String last_name, String email, String password);

}





