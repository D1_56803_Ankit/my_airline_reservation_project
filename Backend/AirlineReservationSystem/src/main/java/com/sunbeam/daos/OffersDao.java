package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sunbeam.entities.Offers;

public interface OffersDao extends JpaRepository<Offers, Integer> {

	Offers findById(int id);
	
	@Modifying
	@Query(value="INSERT into offers(promocode, discount, min_txn_amount) VALUES(?1,?2,?3)", nativeQuery = true)
	int addOffer(String promocode, double discount, double min_txn_amount);

}
