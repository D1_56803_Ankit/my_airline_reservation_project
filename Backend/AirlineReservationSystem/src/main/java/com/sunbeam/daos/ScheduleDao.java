package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.Schedule;

public interface ScheduleDao extends JpaRepository<Schedule, Integer> {
	Schedule findById(int id);
}
