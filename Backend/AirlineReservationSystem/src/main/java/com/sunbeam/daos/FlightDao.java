package com.sunbeam.daos;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sunbeam.entities.Flight;


public interface FlightDao extends JpaRepository<Flight, Integer> {

Flight findById(int id);


@Query(value="SELECT f.flight_id, s.schedule_id ,f.airline_name, f.source_city, f.destination_city, s.departure_date, s.arrival_date, CONCAT(MOD(HOUR(TIMEDIFF(s.arrival_time,s.departure_time)), 24), ' hr ',MINUTE(TIMEDIFF(s.arrival_time,s.departure_time)), ' min ') as duration,"
		+ " f.economy_class_fare, f.business_class_fare FROM flight f INNER JOIN schedule s ON s.flight_id=f.flight_id WHERE source_city=?2 and destination_city =?1 and departure_date=?3", nativeQuery = true)
List<FlightScheduleDtoProjection> searchFlight( String destination,String source, Date departureDate);

@Query(value="SELECT f.flight_id, f.airline_name, f.source_city, f.destination_city, s.departure_date, s.arrival_date, s.departure_time, s.arrival_time, CONCAT(MOD(HOUR(TIMEDIFF(s.arrival_time,s.departure_time)), 24), ' hr ',MINUTE(TIMEDIFF(s.arrival_time,s.departure_time)), ' min ') as duration,"
		+ "s.flight_status FROM flight f INNER JOIN schedule s ON s.flight_id=f.flight_id WHERE f.flight_id=?1 and departure_date=?2", nativeQuery = true)
List<FlightScheduleDtoProjection> checkFlightStatus( int flightid, Date departureDate);
}
