package com.sunbeam.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "flight")
public class Flight {

@GeneratedValue(strategy = GenerationType.IDENTITY)
@Id
@Column(name = "flight_id")
private int id;
private String airline_name;
private String departure_airport_name;
private String arrival_airport_name ;
private String source_city ;
private String destination_city ;
private double business_class_fare;
private double economy_class_fare;
private int business_class_capacity;
private int economy_class_capacity;
private int flight_total_capacity;
@JsonIgnoreProperties("flight")
@OneToMany(mappedBy = "flight", cascade = CascadeType.REMOVE)
private List<Schedule> scheduleList;

public Flight() {
	this.scheduleList=new ArrayList<Schedule>();
}

public Flight(int id, String airline_name, String departure_airport_name, String arrival_airport_name,
		String source_city, String destination_city, double business_class_fare, double economy_class_fare,
		int business_class_capacity, int economy_class_capacity, int flight_total_capacity) {
	this.id = id;
	this.airline_name = airline_name;
	this.departure_airport_name = departure_airport_name;
	this.arrival_airport_name = arrival_airport_name;
	this.source_city = source_city;
	this.destination_city = destination_city;
	this.business_class_fare = business_class_fare;
	this.economy_class_fare = economy_class_fare;
	this.business_class_capacity = business_class_capacity;
	this.economy_class_capacity = economy_class_capacity;
	this.flight_total_capacity = flight_total_capacity;
	this.scheduleList=new ArrayList<Schedule>();
}


public Flight(String airline_name, String departure_airport_name, String arrival_airport_name, String source_city,
		String destination_city, double business_class_fare, double economy_class_fare, int business_class_capacity,
		int economy_class_capacity, int flight_total_capacity) {
	this.airline_name = airline_name;
	this.departure_airport_name = departure_airport_name;
	this.arrival_airport_name = arrival_airport_name;
	this.source_city = source_city;
	this.destination_city = destination_city;
	this.business_class_fare = business_class_fare;
	this.economy_class_fare = economy_class_fare;
	this.business_class_capacity = business_class_capacity;
	this.economy_class_capacity = economy_class_capacity;
	this.flight_total_capacity = flight_total_capacity;
	this.scheduleList=new ArrayList<Schedule>();
}

public List<Schedule> getScheduleList() {
	return scheduleList;
}

public void setScheduleList(List<Schedule> scheduleList) {
	this.scheduleList = scheduleList;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getAirline_name() {
	return airline_name;
}

public void setAirline_name(String airline_name) {
	this.airline_name = airline_name;
}

public String getDeparture_airport_name() {
	return departure_airport_name;
}

public void setDeparture_airport_name(String departure_airport_name) {
	this.departure_airport_name = departure_airport_name;
}

public String getArrival_airport_name() {
	return arrival_airport_name;
}

public void setArrival_airport_name(String arrival_airport_name) {
	this.arrival_airport_name = arrival_airport_name;
}

public String getSource_city() {
	return source_city;
}

public void setSource_city(String source_city) {
	this.source_city = source_city;
}

public String getDestination_city() {
	return destination_city;
}

public void setDestination_city(String destination_city) {
	this.destination_city = destination_city;
}

public double getBusiness_class_fare() {
	return business_class_fare;
}

public void setBusiness_class_fare(double business_class_fare) {
	this.business_class_fare = business_class_fare;
}

public double getEconomy_class_fare() {
	return economy_class_fare;
}

public void setEconomy_class_fare(double economy_class_fare) {
	this.economy_class_fare = economy_class_fare;
}

public int getBusiness_class_capacity() {
	return business_class_capacity;
}

public void setBusiness_class_capacity(int business_class_capacity) {
	this.business_class_capacity = business_class_capacity;
}

public int getEconomy_class_capacity() {
	return economy_class_capacity;
}

public void setEconomy_class_capacity(int economy_class_capacity) {
	this.economy_class_capacity = economy_class_capacity;
}

public int getFlight_total_capacity() {
	return flight_total_capacity;
}

public void setFlight_total_capacity(int flight_total_capacity) {
	this.flight_total_capacity = flight_total_capacity;
}

@Override
public String toString() {
	return "Flight [id=" + id + ", airline_name=" + airline_name + ", departure_airport_name=" + departure_airport_name
			+ ", arrival_airport_name=" + arrival_airport_name + ", source_city=" + source_city + ", destination_city="
			+ destination_city + ", business_class_fare=" + business_class_fare + ", economy_class_fare="
			+ economy_class_fare + ", business_class_capacity=" + business_class_capacity + ", economy_class_capacity="
			+ economy_class_capacity + ", flight_total_capacity=" + flight_total_capacity + "]";
}


}
