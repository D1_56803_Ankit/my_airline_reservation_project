package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "offers ")
public class Offers {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "offer_id")
	private int id;
	private String promocode;
	private double discount;
	private double  min_txn_amount;
	private String valid_on;
	
	public Offers() {
	}

	public Offers(int id, String promocode, double discount, double min_txn_amount) {
		this.id = id;
		this.promocode = promocode;
		this.discount = discount;
		this.min_txn_amount = min_txn_amount;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPromocode() {
		return promocode;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getMin_txn_amount() {
		return min_txn_amount;
	}

	public void setMin_txn_amount(double min_txn_amount) {
		this.min_txn_amount = min_txn_amount;
	}

	public String getValid_on() {
		return valid_on;
	}

	public void setValid_on(String valid_on) {
		this.valid_on = valid_on;
	}

	@Override
	public String toString() {
		return "Offers [id=" + id + ", promocode=" + promocode + ", discount=" + discount + ", min_txn_amount="
				+ min_txn_amount + ", valid_on=" + valid_on + "]";
	}

	
	
}
