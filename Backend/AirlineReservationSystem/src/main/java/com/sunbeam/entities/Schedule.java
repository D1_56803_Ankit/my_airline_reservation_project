package com.sunbeam.entities;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "schedule")
public class Schedule {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "schedule_id")
	private int id;
	private Date departure_date;
	//@Temporal(TemporalType.TIME)
	private Time departure_time;
	private Date arrival_date ;
	//@Temporal(TemporalType.TIME)
	private Time arrival_time ;
	private String flight_status;
	@ManyToOne
	@JoinColumn(name = "flight_id")
	@JsonIgnoreProperties("scheduleList")
	//@JsonManagedReference
	private Flight flight;

	public Schedule() {
	}

	public Schedule(int id, Date departure_date, Time departure_time, Date arrival_date, Time arrival_time,
			String flight_status) {
		this.id = id;
		this.departure_date = departure_date;
		this.departure_time = departure_time;
		this.arrival_date = arrival_date;
		this.arrival_time = arrival_time;
		this.flight_status = flight_status;
	}



	public Schedule(Date departure_date, Time departure_time, Date arrival_date, Time arrival_time,
			String flight_status) {
		this.departure_date = departure_date;
		this.departure_time = departure_time;
		this.arrival_date = arrival_date;
		this.arrival_time = arrival_time;
		this.flight_status = flight_status;
	}


	public int getId() {
		return id;
	}

	public Date getDeparture_date() {
		return departure_date;
	}

	public void setDeparture_date(Date departure_date) {
		this.departure_date = departure_date;
	}

	public Time getDeparture_time() {
		return departure_time;
	}

	public void setDeparture_time(Time time) {
		this.departure_time = time;
	}

	public Date getArrival_date() {
		return arrival_date;
	}

	public void setArrival_date(Date arrival_date) {
		this.arrival_date = arrival_date;
	}

	public Time getArrival_time() {
		return arrival_time;
	}

	public void setArrival_time(Time arrival_time) {
		this.arrival_time = arrival_time;
	}

	public String getFlight_status() {
		return flight_status;
	}

	public void setFlight_status(String flight_status) {
		this.flight_status = flight_status;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Schedule [id=" + id + ", departure_date=" + departure_date + ", departure_time=" + departure_time
				+ ", arrival_date=" + arrival_date + ", arrival_time=" + arrival_time + ", flight_status="
				+ flight_status + "]";
	}
	
}
