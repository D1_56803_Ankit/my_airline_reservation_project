package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "package")
public class Package {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "package_id")
	private int id;
	private String package_name;
	private String seat_type;
	private String food;
	private String beverages;
	private String baggage;
	private double package_fare;

	public Package() {
	}

	public Package(String package_name, String seat_type, String food, String beverages, String baggage,
			double package_fare) {
		this.package_name = package_name;
		this.seat_type = seat_type;
		this.food = food;
		this.beverages = beverages;
		this.baggage = baggage;
		this.package_fare = package_fare;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getPackage_name() {
		return package_name;
	}

	public void setPackage_name(String package_name) {
		this.package_name = package_name;
	}

	public String getSeat_type() {
		return seat_type;
	}

	public void setSeat_type(String seat_type) {
		this.seat_type = seat_type;
	}

	public String getFood() {
		return food;
	}

	public void setFood(String food) {
		this.food = food;
	}

	public String getBeverages() {
		return beverages;
	}

	public void setBeverages(String beverages) {
		this.beverages = beverages;
	}

	public String getBaggage() {
		return baggage;
	}

	public void setBaggage(String baggage) {
		this.baggage = baggage;
	}

	public double getPackage_fare() {
		return package_fare;
	}

	public void setPackage_fare(double package_fare) {
		this.package_fare = package_fare;
	}

	@Override
	public String toString() {
		return "Package [id=" + id + ", package_name=" + package_name + ", seat_type=" + seat_type + ", food=" + food
				+ ", beverages=" + beverages + ", baggage=" + baggage + ", package_fare=" + package_fare + "]";
	}


}
