package com.sunbeam.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "passenger")
public class Passenger {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "passenger_id")
	private int id;
	private String title ;
	private String first_name;
	private String last_name;
	private long mobile_no;
	@Email
	private String email;
	private String country;
	private String state;
	private String city;
	private String address;
	private Date date_of_birth;
	private short covid_vaccinated;
	
	public Passenger() {
		
	}

	public Passenger(int id, String title, String first_name, String last_name, long mobile_no, String email,
			String country, String state, String city, String address, Date date_of_birth, short covid_vaccinated) {
		this.id = id;
		this.title = title;
		this.first_name = first_name;
		this.last_name = last_name;
		this.mobile_no = mobile_no;
		this.email = email;
		this.country = country;
		this.state = state;
		this.city = city;
		this.address = address;
		this.date_of_birth = date_of_birth;
		this.covid_vaccinated = covid_vaccinated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public long getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(long mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public short getCovid_vaccinated() {
		return covid_vaccinated;
	}

	public void setCovid_vaccinated(short covid_vaccinated) {
		this.covid_vaccinated = covid_vaccinated;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", title=" + title + ", first_name=" + first_name + ", last_name=" + last_name
				+ ", mobile_no=" + mobile_no + ", email=" + email + ", country=" + country + ", state=" + state
				+ ", city=" + city + ", address=" + address + ", date_of_birth=" + date_of_birth + ", covid_vaccinated="
				+ covid_vaccinated + "]";
	}

	
	
}
