package com.sunbeam.entities;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payment_card_details")
public class PaymentCardDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "card_id")
	private int id;
	//@ManyToOne(cascade = CascadeType.ALL)
	//@JoinColumn(name = "user_id")
	//private User user;
	private int user_id;
	private String name_on_card;
	private long card_no;
	private Date validity_date;
	private int cvv;
	
	public PaymentCardDetails() {
	}

	public PaymentCardDetails(int id, int user_id, String name_on_card, long card_no, Date validity_date, int cvv) {
		this.id = id;
		this.user_id = user_id;
		this.name_on_card = name_on_card;
		this.card_no = card_no;
		this.validity_date = validity_date;
		this.cvv = cvv;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getName_on_card() {
		return name_on_card;
	}

	public void setName_on_card(String name_on_card) {
		this.name_on_card = name_on_card;
	}

	public long getCard_no() {
		return card_no;
	}

	public void setCard_no(long card_no) {
		this.card_no = card_no;
	}

	public Date getValidity_date() {
		return validity_date;
	}

	public void setValidity_date(Date validity_date) {
		this.validity_date = validity_date;
	}

	public int getCvv() {
		return cvv;
	}

	public void setCvv(int cvv) {
		this.cvv = cvv;
	}

	@Override
	public String toString() {
		return "PaymentCardDetails [id=" + id + ", user_id=" + user_id + ", name_on_card=" + name_on_card + ", card_no="
				+ card_no + ", validity_date=" + validity_date + ", cvv=" + cvv + "]";
	}


	
	
}
