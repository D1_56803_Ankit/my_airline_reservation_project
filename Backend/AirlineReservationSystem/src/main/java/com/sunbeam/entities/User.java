package com.sunbeam.entities;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="user")
public class User {
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Id
@Column(name = "user_id")
private int id;
private String first_name;
private String last_name;
@Email
@NotBlank
private String email;
private String password;
private String role;
@OneToMany(mappedBy = "user_id")
private List<PaymentCardDetails> listOfCards;
public User() {
	
}

public User(int id) {

	this.id = id;
}


public User(int id, String first_name, String last_name, String email, String password, String role) {

	this.id = id;
	this.first_name = first_name;
	this.last_name = last_name;
	this.email = email;
	this.password = password;
	this.role = role;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getFirst_name() {
	return first_name;
}

public void setFirst_name(String first_name) {
	this.first_name = first_name;
}

public String getLast_name() {
	return last_name;
}

public void setLast_name(String last_name) {
	this.last_name = last_name;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getRole() {
	return role;
}

public void setRole(String role) {
	this.role = role;
}

@Override
public String toString() {
	return "User [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name + ", email=" + email
			+ ", password=" + password + ", role=" + role + "]";
}

}


