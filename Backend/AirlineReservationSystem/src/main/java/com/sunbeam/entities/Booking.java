package com.sunbeam.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "booking")
public class Booking {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "booking_id")
	private int id ;

	private Date booking_date;
	private int passenger_count;
	private double fare_per_passenger ;
	private int seat_no;
	private double total_fare ;
	private String ticket_status;

	@ManyToOne
	@JoinColumn(name = "passenger_id")
	private Passenger passenger;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "schedule_id")
	private Schedule schedule;
	
	@ManyToOne
	@JoinColumn(name = "offer_id")
	private Offers offer;
	
	@ManyToOne
	@JoinColumn(name = "package_id")
	private Package packages;
	
	@ManyToOne
	@JoinColumn(name = "flight_id")
	private Flight flight;

	public Booking() {
		
	}

	public Booking(int id, Date booking_date, int passenger_count, double fare_per_passenger, int seat_no,
			double total_fare, String ticket_status) {
		this.id = id;
		this.booking_date = booking_date;
		this.passenger_count = passenger_count;
		this.fare_per_passenger = fare_per_passenger;
		this.seat_no = seat_no;
		this.total_fare = total_fare;
		this.ticket_status = ticket_status;
	}

	public Booking(Date booking_date, int passenger_count, double fare_per_passenger, int seat_no,
			double total_fare, String ticket_status) {
		this.booking_date = booking_date;
		this.passenger_count = passenger_count;
		this.fare_per_passenger = fare_per_passenger;
		this.seat_no = seat_no;
		this.total_fare = total_fare;
		this.ticket_status = ticket_status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBooking_date() {
		return booking_date;
	}

	public void setBooking_date(Date booking_date) {
		this.booking_date = booking_date;
	}

	public int getPassenger_count() {
		return passenger_count;
	}

	public void setPassenger_count(int passenger_count) {
		this.passenger_count = passenger_count;
	}

	public double getFare_per_passenger() {
		return fare_per_passenger;
	}

	public void setFare_per_passenger(double fare_per_passenger) {
		this.fare_per_passenger = fare_per_passenger;
	}

	public int getSeat_no() {
		return seat_no;
	}

	public void setSeat_no(int seat_no) {
		this.seat_no = seat_no;
	}

	public double getTotal_fare() {
		return total_fare;
	}

	public void setTotal_fare(double total_fare) {
		this.total_fare = total_fare;
	}

	public String getTicket_status() {
		return ticket_status;
	}

	public void setTicket_status(String ticket_status) {
		this.ticket_status = ticket_status;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public Offers getOffer() {
		return offer;
	}

	public void setOffer(Offers offer) {
		this.offer = offer;
	}

	public Package getPackages() {
		return packages;
	}

	public void setPackages(Package packages) {
		this.packages = packages;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	@Override
	public String toString() {
		return "Booking [id=" + id + ", booking_date=" + booking_date + ", passenger_count=" + passenger_count
				+ ", fare_per_passenger=" + fare_per_passenger + ", seat_no=" + seat_no + ", total_fare=" + total_fare
				+ ", ticket_status=" + ticket_status + "]";
	}
	
}
