package com.sunbeam.dtos;

import org.springframework.stereotype.Component;

import com.sunbeam.entities.Flight;
import com.sunbeam.entities.Schedule;

@Component
public class DtoToEntityConverter {

	public Flight toFlightEntity(FlightDto fDto) {
		Flight entityFlight=new Flight();
		entityFlight.setId(fDto.getFlightId());
		entityFlight.setAirline_name(fDto.getAirlineName());
		entityFlight.setDeparture_airport_name(fDto.getDepartureAirportName());
		entityFlight.setArrival_airport_name(fDto.getArrivalAirportName());
		entityFlight.setSource_city(fDto.getSourceCity());
		entityFlight.setDestination_city(fDto.getDestinationCity());
		entityFlight.setBusiness_class_fare(fDto.getBusinessClassFare());
		entityFlight.setEconomy_class_fare(fDto.getEconomyClassFare());
		entityFlight.setBusiness_class_capacity(fDto.getBusinessClassCapacity());
		entityFlight.setEconomy_class_capacity(fDto.getEconomyClassCapacity());
		entityFlight.setFlight_total_capacity(fDto.getFlightTotalCapacity());
		return entityFlight;
	}
	
	public Schedule toScheduleEntity(FlightDto flightDto) {
		Schedule entity=new Schedule();
		entity.setDeparture_date(flightDto.getDepartureDate());
		entity.setDeparture_time(flightDto.getDepartureTime());
		entity.setArrival_date(flightDto.getArrivalDate());
		entity.setArrival_time(flightDto.getArrivalTime());
		entity.setFlight_status(flightDto.getFlightStatus());
		return entity;
	}


}
