package com.sunbeam.dtos;

import java.sql.Date;
import java.sql.Time;

import org.springframework.stereotype.Component;
@Component
public class ScheduleDto {
	
	private int flight_id;
	private Date departure_date;
	private Date arrival_date ;
	private Time departure_time;
	private Time arrival_time ;
	private String flight_status;
		
	public ScheduleDto() {
	}

	public ScheduleDto(int flight_id, Date departure_date, Date arrival_date, Time departure_time, Time arrival_time) {
		this.flight_id = flight_id;
		this.departure_date = departure_date;
		this.arrival_date = arrival_date;
		this.departure_time = departure_time;
		this.arrival_time = arrival_time;
	}

	public ScheduleDto(int flight_id, Date departure_date, Date arrival_date, Time departure_time, Time arrival_time,
			String flight_status) {
		this.flight_id = flight_id;
		this.departure_date = departure_date;
		this.arrival_date = arrival_date;
		this.departure_time = departure_time;
		this.arrival_time = arrival_time;
		this.flight_status = flight_status;
	}

	public int getFlight_id() {
		return flight_id;
	}

	public void setFlight_id(int flight_id) {
		this.flight_id = flight_id;
	}

	public Date getDeparture_date() {
		return departure_date;
	}

	public void setDeparture_date(Date departure_date) {
		this.departure_date = departure_date;
	}

	public Date getArrival_date() {
		return arrival_date;
	}

	public void setArrival_date(Date arrival_date) {
		this.arrival_date = arrival_date;
	}

	public Time getDeparture_time() {
		return departure_time;
	}

	public void setDeparture_time(Time departure_time) {
		this.departure_time = departure_time;
	}

	public Time getArrival_time() {
		return arrival_time;
	}

	public void setArrival_time(Time arrival_time) {
		this.arrival_time = arrival_time;
	}

	public String getFlight_status() {
		return flight_status;
	}

	public void setFlight_status(String flight_status) {
		this.flight_status = flight_status;
	}

	@Override
	public String toString() {
		return "ScheduleDto [flight_id=" + flight_id + ", departure_date=" + departure_date + ", arrival_date="
				+ arrival_date + ", departure_time=" + departure_time + ", arrival_time=" + arrival_time
				+ ", flight_status=" + flight_status + "]";
	}
	
	
}
