package com.sunbeam.dtos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

public class FlightScheduleDto {
	private int flightId;
	private String airlineName;
	private int scheduleId;
	@Temporal(TemporalType.TIME)
	private Date departureTime;
	@Temporal(TemporalType.TIME)
	private Date arrivalTime;
	private String duration;
	private double businessClassFare;
	private double economyClassFare;
	
	public FlightScheduleDto() {
		super();
	}
	
	public FlightScheduleDto(int flightId, String airlineName, int scheduleId, Date departureTime, Date arrivalTime,
			String duration, double businessClassFare, double economyClassFare) {
		this.flightId = flightId;
		this.airlineName = airlineName;
		this.scheduleId = scheduleId;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.duration = duration;
		this.businessClassFare = businessClassFare;
		this.economyClassFare = economyClassFare;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public double getBusinessClassFare() {
		return businessClassFare;
	}

	public void setBusinessClassFare(double businessClassFare) {
		this.businessClassFare = businessClassFare;
	}

	public double getEconomyClassFare() {
		return economyClassFare;
	}

	public void setEconomyClassFare(double economyClassFare) {
		this.economyClassFare = economyClassFare;
	}

	@Override
	public String toString() {
		return "FlightScheduleDto [flightId=" + flightId + ", airlineName=" + airlineName + ", scheduleId=" + scheduleId
				+ ", departureTime=" + departureTime + ", arrivalTime=" + arrivalTime + ", duration=" + duration
				+ ", businessClassFare=" + businessClassFare + ", economyClassFare=" + economyClassFare + "]";
	}
	
	

}
