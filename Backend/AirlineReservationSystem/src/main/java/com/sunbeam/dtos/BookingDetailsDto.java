package com.sunbeam.dtos;

import java.sql.Date;
import java.sql.Time;

public class BookingDetailsDto {

	private int booking_id;
	private String ticket_status;
	private int seat_no;
	private double total_fare;
	private Date booking_date;
	private int flight_id;
	private String source_city;
	private String destination_city;
	private String departure_airport_name;
	private String arrival_airport_name;
	private Time arrival_time;
	private Time departure_time;
	private Date arrival_date;
	private Date departure_date;
	private String first_name;
	private String last_name;
	
	
	public BookingDetailsDto() {
		super();
	}


	public BookingDetailsDto(int booking_id, String ticket_status, int seat_no, double total_fare, Date booking_date,
			int flight_id, String source_city, String destination_city, String departure_airport_name,
			String arrival_airport_name, Time arrival_time, Time departure_time, Date arrival_date, Date departure_date,
			String first_name, String last_name) {
		super();
		this.booking_id = booking_id;
		this.ticket_status = ticket_status;
		this.seat_no = seat_no;
		this.total_fare = total_fare;
		this.booking_date = booking_date;
		this.flight_id = flight_id;
		this.source_city = source_city;
		this.destination_city = destination_city;
		this.departure_airport_name = departure_airport_name;
		this.arrival_airport_name = arrival_airport_name;
		this.arrival_time = arrival_time;
		this.departure_time = departure_time;
		this.arrival_date = arrival_date;
		this.departure_date = departure_date;
		this.first_name = first_name;
		this.last_name = last_name;
	}


	public int getBooking_id() {
		return booking_id;
	}


	public void setBooking_id(int booking_id) {
		this.booking_id = booking_id;
	}


	public String getTicket_status() {
		return ticket_status;
	}


	public void setTicket_status(String ticket_status) {
		this.ticket_status = ticket_status;
	}


	public int getSeat_no() {
		return seat_no;
	}


	public void setSeat_no(int seat_no) {
		this.seat_no = seat_no;
	}


	public double getTotal_fare() {
		return total_fare;
	}


	public void setTotal_fare(double total_fare) {
		this.total_fare = total_fare;
	}


	public Date getBooking_date() {
		return booking_date;
	}


	public void setBooking_date(Date booking_date) {
		this.booking_date = booking_date;
	}


	public int getFlight_id() {
		return flight_id;
	}


	public void setFlight_id(int flight_id) {
		this.flight_id = flight_id;
	}


	public String getSource_city() {
		return source_city;
	}


	public void setSource_city(String source_city) {
		this.source_city = source_city;
	}


	public String getDestination_city() {
		return destination_city;
	}


	public void setDestination_city(String destination_city) {
		this.destination_city = destination_city;
	}


	public String getDeparture_airport_name() {
		return departure_airport_name;
	}


	public void setDeparture_airport_name(String departure_airport_name) {
		this.departure_airport_name = departure_airport_name;
	}


	public String getArrival_airport_name() {
		return arrival_airport_name;
	}


	public void setArrival_airport_name(String arrival_airport_name) {
		this.arrival_airport_name = arrival_airport_name;
	}


	public Time getArrival_time() {
		return arrival_time;
	}


	public void setArrival_time(Time arrival_time) {
		this.arrival_time = arrival_time;
	}


	public Time getDeparture_time() {
		return departure_time;
	}


	public void setDeparture_time(Time departure_time) {
		this.departure_time = departure_time;
	}


	public Date getArrival_date() {
		return arrival_date;
	}


	public void setArrival_date(Date arrival_date) {
		this.arrival_date = arrival_date;
	}


	public Date getDeparture_date() {
		return departure_date;
	}


	public void setDeparture_date(Date departure_date) {
		this.departure_date = departure_date;
	}


	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	@Override
	public String toString() {
		return "BookingDetailsDto [booking_id=" + booking_id + ", ticket_status=" + ticket_status + ", seat_no="
				+ seat_no + ", total_fare=" + total_fare + ", booking_date=" + booking_date + ", flight_id=" + flight_id
				+ ", source_city=" + source_city + ", destination_city=" + destination_city
				+ ", departure_airport_name=" + departure_airport_name + ", arrival_airport_name="
				+ arrival_airport_name + ", arrival_time=" + arrival_time + ", departure_time=" + departure_time
				+ ", arrival_date=" + arrival_date + ", departure_date=" + departure_date + ", first_name=" + first_name
				+ ", last_name=" + last_name + "]";
	}
	

}
