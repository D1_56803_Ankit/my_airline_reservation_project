package com.sunbeam.dtos;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

import com.sunbeam.entities.Schedule;

@Component
public class FlightDto {


	private int flightId;
	private String airlineName;
	private String departureAirportName;
	private String arrivalAirportName;
	private String sourceCity;
	private String destinationCity;
	private double businessClassFare;
	private double economyClassFare;
	private int businessClassCapacity;
	private int economyClassCapacity;
	private int flightTotalCapacity;
	private Date departureDate;
	
	//@Temporal(TemporalType.TIME)
	private Time departureTime;
	
	private Date arrivalDate;
	
	//@Temporal(TemporalType.TIME)
	private Time arrivalTime;
	
	private String flightStatus;
	private List<Schedule> scheduleList;
	
	public FlightDto() {
		this.scheduleList=new ArrayList<Schedule>();
	}
	public FlightDto(int flightId, String airlineName, String departureAirportName, String arrivalAirportName,
			String sourceCity, String destinationCity, double businessClassFare, double economyClassFare,
			int businessClassCapacity, int economyClassCapacity, int flightTotalCapacity, Date departureDate,
			Time departureTime, Date arrivalDate, Time arrivalTime, String flightStatus) {
		super();
		this.flightId = flightId;
		this.airlineName = airlineName;
		this.departureAirportName = departureAirportName;
		this.arrivalAirportName = arrivalAirportName;
		this.sourceCity = sourceCity;
		this.destinationCity = destinationCity;
		this.businessClassFare = businessClassFare;
		this.economyClassFare = economyClassFare;
		this.businessClassCapacity = businessClassCapacity;
		this.economyClassCapacity = economyClassCapacity;
		this.flightTotalCapacity = flightTotalCapacity;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
		this.arrivalDate = arrivalDate;
		this.arrivalTime = arrivalTime;
		this.flightStatus = flightStatus;
	}
	
	
	public FlightDto(String sourceCity, String destinationCity, Date departureDate) {
		this.sourceCity = sourceCity;
		this.destinationCity = destinationCity;
		this.departureDate = departureDate;
	}
	public int getFlightId() {
		return flightId;
	}
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getDepartureAirportName() {
		return departureAirportName;
	}
	public void setDepartureAirportName(String departureAirportName) {
		this.departureAirportName = departureAirportName;
	}
	public String getArrivalAirportName() {
		return arrivalAirportName;
	}
	public void setArrivalAirportName(String arrivalAirportName) {
		this.arrivalAirportName = arrivalAirportName;
	}
	public String getSourceCity() {
		return sourceCity;
	}
	public void setSourceCity(String sourceCity) {
		this.sourceCity = sourceCity;
	}
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public double getBusinessClassFare() {
		return businessClassFare;
	}
	public void setBusinessClassFare(double businessClassFare) {
		this.businessClassFare = businessClassFare;
	}
	public double getEconomyClassFare() {
		return economyClassFare;
	}
	public void setEconomyClassFare(double economyClassFare) {
		this.economyClassFare = economyClassFare;
	}
	public int getBusinessClassCapacity() {
		return businessClassCapacity;
	}
	public void setBusinessClassCapacity(int businessClassCapacity) {
		this.businessClassCapacity = businessClassCapacity;
	}
	public int getEconomyClassCapacity() {
		return economyClassCapacity;
	}
	public void setEconomyClassCapacity(int economyClassCapacity) {
		this.economyClassCapacity = economyClassCapacity;
	}
	public int getFlightTotalCapacity() {
		return flightTotalCapacity;
	}
	public void setFlightTotalCapacity(int flightTotalCapacity) {
		this.flightTotalCapacity = flightTotalCapacity;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public Time getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Time departureTime) {
		this.departureTime = departureTime;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Time getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Time arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getFlightStatus() {
		return flightStatus;
	}
	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}
	public List<Schedule> getScheduleList() {
		return scheduleList;
	}
	public void setScheduleList(List<Schedule> scheduleList) {
		this.scheduleList = scheduleList;
	}
	@Override
	public String toString() {
		return "FlightDto [flightId=" + flightId + ", airlineName=" + airlineName + ", departureAirportName="
				+ departureAirportName + ", arrivalAirportName=" + arrivalAirportName + ", sourceCity=" + sourceCity
				+ ", destinationCity=" + destinationCity + ", businessClassFare=" + businessClassFare
				+ ", economyClassFare=" + economyClassFare + ", businessClassCapacity=" + businessClassCapacity
				+ ", economyClassCapacity=" + economyClassCapacity + ", flightTotalCapacity=" + flightTotalCapacity
				+ ", departureDate=" + departureDate + ", departureTime=" + departureTime + ", arrivalDate="
				+ arrivalDate + ", arrivalTime=" + arrivalTime + ", flightStatus=" + flightStatus + "]";
	}
		
	
	

}
