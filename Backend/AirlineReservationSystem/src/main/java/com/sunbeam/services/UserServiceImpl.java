
package com.sunbeam.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.UserDao;
import com.sunbeam.entities.User;

@Transactional
@Service
public class UserServiceImpl {
@Autowired
private UserDao userDao;

public User findUserById (int userId)
{
	return userDao.findById(userId);
}

public List<User> findAllUsers() {
	return userDao.findAll();
}

public User findUserByEmail (String email) {
	return userDao.findByEmail(email);
}

public int addUser(User user) {
	return userDao.addUser(user.getFirst_name(), user.getLast_name(), user.getEmail(), user.getPassword());
}

public User updateUser(User user) {
System.out.println("In service"+user);
	return userDao.save(user);
}

public int deleteUserById(int id) {
	
	if(userDao.existsById(id))
	{
		userDao.deleteById(id);
		return 1;
	}
	else 
		return 0;
}

}

