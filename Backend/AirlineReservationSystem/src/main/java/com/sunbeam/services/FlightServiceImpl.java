package com.sunbeam.services;

import java.sql.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.FlightDao;
import com.sunbeam.daos.FlightScheduleDtoProjection;
import com.sunbeam.dtos.FlightDto;
import com.sunbeam.dtos.FlightScheduleDto;
import com.sunbeam.entities.Flight;

@Transactional
@Service
public class FlightServiceImpl {

	@Autowired
	private FlightDao flightDao;

//Find all
	public List<Flight> findAllFlight() {
		return flightDao.findAll();
	}

//Add Flight
	public Flight addFlight(Flight flight) {
		return flightDao.save(flight);
	}

//Find Flight by id
	public Flight findFlightById(int id) {
		return flightDao.findById(id);
	}
	
//Search flight
	public List<FlightScheduleDtoProjection> searchFlight(String destination, String source, Date departureDate){
		List<FlightScheduleDtoProjection> list= flightDao.searchFlight(destination, source, departureDate);
		if(list.isEmpty()) 
		return null;
			return list;
	}

//Check flight status
		public List<FlightScheduleDtoProjection> checkFlightStatus(int flightid, Date departureDate){
			List<FlightScheduleDtoProjection> list= flightDao.checkFlightStatus(flightid, departureDate);
			if(list.isEmpty()) 
			return null;
				return list;
	}	
	
// Update Flight
	public Flight updateFlight(Flight flight) {

		return flightDao.save(flight);
	}

//Delete Flight By id
	public int deletePassenger(int id) {
		if (flightDao.existsById(id)) {
			flightDao.deleteById(id);
			return 1;
		} else
			return 0;
	}
}
