package com.sunbeam.services;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.PassengerDao;
import com.sunbeam.entities.Passenger;

@Transactional
@Service
public class PassengerServiceImpl {

	@Autowired
	private PassengerDao passengerDao;

	//Find passenger by id
	public Passenger findPassengerById(int id)
	{
		return passengerDao.findById(id);
	}

	//Add passenger
	public Passenger addPassenger(Passenger passenger)
	{
		return passengerDao.save(passenger);
	}
	
	public Passenger saveOrUpdate(Passenger p) {
		Passenger p2=passengerDao.findByFirstNameAndLastNameAndEmail(p.getFirst_name(), p.getFirst_name(), p.getEmail());
		System.out.println("Before "+p2);
		if(p2==null) {
			System.out.println("inside if-condition");
			return passengerDao.save(p);
		}else {
			p2.setMobile_no(p.getMobile_no());
			p2.setDate_of_birth(p.getDate_of_birth());
			p2.setCovid_vaccinated(p.getCovid_vaccinated());
			p2.setAddress(p.getAddress());
			p2.setCity(p.getCity());
			p2.setState(p.getState());
			p2.setCountry(p.getCountry());
			System.out.println("After "+p2);
			
			return passengerDao.save(p2);
		}
	}

	
	//find all passengers
	public List<Passenger> findall(){
		return passengerDao.findAll();
	}

	// Update passenger
	public Passenger updatePassenger(Passenger passenger) {
		
		return passengerDao.save(passenger);
	}
	
	//Delete Passenger By id
	public int deletePassenger(int id) {
		if(passengerDao.existsById(id))
		{
			passengerDao.deleteById(id);
		return 1;
		}else
			return 0;
	}
}
