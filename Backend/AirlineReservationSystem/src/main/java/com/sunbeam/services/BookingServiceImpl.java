package com.sunbeam.services;

import java.sql.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sunbeam.daos.BookingDao;
import com.sunbeam.daos.BookingDetailsProjection;
import com.sunbeam.daos.FlightDao;
import com.sunbeam.daos.PassengerDao;
import com.sunbeam.daos.ScheduleDao;
import com.sunbeam.daos.UserDao;
import com.sunbeam.dtos.BookingDto;
import com.sunbeam.dtos.RandomNoGenerator;
import com.sunbeam.entities.Booking;

@Transactional
@Service
public class BookingServiceImpl {

	@Autowired
	private BookingDao bookingDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private PassengerDao passengerDao;
	
	@Autowired
	private FlightDao flightDao;
	
	@Autowired
	private ScheduleDao scheduleDao;
	

// Get Booking by Id
		public Booking findBookingById(int bookingId) {
			return bookingDao.findById(bookingId);
		}
		
//Get all Bookings
		public List<Booking> findAllBookigs() {
			return bookingDao.findAll();
		}

//Add Booking 
		public Booking addBooking(BookingDto bookingDto) {
			List<Integer> list=bookingDao.findSeatNo(bookingDto.getScheduleId());
			System.out.println(list);
			long sec=System.currentTimeMillis();
			
			Date newDate = new Date(sec);
			System.out.println(newDate);
			Booking booking=new Booking();
			booking.setBooking_date(newDate);
			booking.setTotal_fare(bookingDto.getTotalFare());
			booking.setUser(userDao.findById(bookingDto.getUserId()));
			booking.setPassenger(passengerDao.findById(bookingDto.getPassengerId()));
			booking.setFlight(flightDao.findById(bookingDto.getFlightId()));
			booking.setSchedule(scheduleDao.findById(bookingDto.getScheduleId()));
			booking.setSeat_no(RandomNoGenerator.getRandomNonRepeatingIntegers(bookingDao.findSeatNo(bookingDto.getScheduleId()), 1, 20));
			booking.setTicket_status("Booked");
			return bookingDao.save(booking);

		}


// Get booking details for tickit generation	
		public List<BookingDetailsProjection> getAllBookingDetails() {
			return bookingDao.getBookingDetails();
			
		}
		
// Get booking details by booking id for tickit generation	
		public BookingDetailsProjection getBookingDetailsById(int bookingId) {
			return bookingDao.getBookingDetailsById(bookingId);
					
		}

// Get booking details by User id for tickit generation	
		public List<BookingDetailsProjection> findMyBooking(int userId) {
			return bookingDao.findMyBooking(userId);
							
		}
//Check In
		public Object checkIn(int bookingId, String firstName) {
			Booking b= bookingDao.findByBookingIdAndFirstName(bookingId, firstName);
			System.out.println(b);
			if(b==null) {
				return null;
			}
			else if(b.getTicket_status().equals("Confirmed")) {
				return "Confirmed";
			}else if(b.getTicket_status().equals( "Cancelled")){
				return "Cancelled";
			}else {
				b.setTicket_status("Confirmed");
				return bookingDao.save(b);
			}
			}
		
//My Trip
		public Booking myTrip(int bookingId, String firstName) {
			
			return bookingDao.findByBookingIdAndFirstName(bookingId, firstName);
		}

		
//Cancel booking by booking Id		
		public Object cancelBooking(int bookingId) {
			Booking b = bookingDao.findBookingById(bookingId);
			if(b!=null)
			{
				if(b.getTicket_status().equals("Confirmed") || b.getTicket_status().equals("Booked"))
					{
					b.setTicket_status("Cancelled");
					System.out.println(b.getTicket_status());
					return bookingDao.save(b);
					}
				else		
					return "Tickit is already cancelled";
			}
		return null;
		}

	
}
