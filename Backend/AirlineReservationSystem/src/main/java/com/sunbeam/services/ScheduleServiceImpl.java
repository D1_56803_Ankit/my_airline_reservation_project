package com.sunbeam.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.FlightDao;
import com.sunbeam.daos.ScheduleDao;
import com.sunbeam.dtos.ScheduleDto;
import com.sunbeam.entities.Flight;
import com.sunbeam.entities.Schedule;

@Transactional
@Service
public class ScheduleServiceImpl {

	@Autowired
	private ScheduleDao scheduleDao;
	@Autowired
	private FlightDao flightDao;
	
	//Add schedule
	public Schedule addSchedule(ScheduleDto schedule) 
	{
		System.out.println(schedule);
		Schedule s=new Schedule();
		s.setDeparture_date(schedule.getDeparture_date());
		s.setArrival_date(schedule.getArrival_date());
		s.setDeparture_time(schedule.getDeparture_time());
		s.setArrival_time(schedule.getArrival_time());
		s.setFlight_status("scheduled");
		Flight f=flightDao.findById(schedule.getFlight_id());
		s.setFlight(f);
		return scheduleDao.save(s);
	}
	
	//Find all schedules
	public List<Schedule> findAll()
	{
		return scheduleDao.findAll();
	}
	
	//Find schedule by id
		public Schedule findSchedule(int id)
		{
			return scheduleDao.findById(id);
		}
		
		// Update schedule
		public Schedule updateSchedule(Schedule schedule) {
				
				return scheduleDao.save(schedule);
		}
			
	//Delete schedule By id
		public int deleteSchedule(int id) {
		    if(scheduleDao.existsById(id))
			{
		    	scheduleDao.deleteById(id);
			  return 1;
			}else
				return 0;
		}
}
