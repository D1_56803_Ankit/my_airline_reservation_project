package com.sunbeam.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sunbeam.daos.PackageDao;
import com.sunbeam.entities.Package;

@Transactional
@Service
public class PackageServiceImpl {
	@Autowired
	private PackageDao packageDao;

	// Find by id
	public Package findpackageById(int packageId) {
		return packageDao.findById(packageId);
	}

	// find all Package
	public List<Package> findAllPackages() {
		return packageDao.findAll();
	}

	// Add package
	public Package addPackage(Package Package) {
		return packageDao.save(Package);
	}

	// Update package
	public Package updatePackage(Package Package) {

		return packageDao.save(Package);
	}

	// Delete package
	public int deletePackageById(int id) {

		if (packageDao.existsById(id)) {
			packageDao.deleteById(id);
			return 1;
		} else
			return 0;
	}

}
