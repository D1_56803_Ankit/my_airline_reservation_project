package com.sunbeam.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.PaymentCardDao;
import com.sunbeam.entities.PaymentCardDetails;

@Transactional
@Service
public class PaymentCardServiceImpl {

	@Autowired
	private PaymentCardDao paymentCardDao;

//Find all
	public List<PaymentCardDetails> findAllPaymentCardDetails() {
		return paymentCardDao.findAll();
	}

//Add PaymentCardDetails
	public PaymentCardDetails addPaymentCardDetails(PaymentCardDetails paymentCardDetails) {
		return paymentCardDao.save(paymentCardDetails);
	}

//Find PaymentCardDetails by id
	public PaymentCardDetails findPaymentCardDetailsById(int id) {
		return paymentCardDao.findById(id);
	}
	
//Find PaymentCardDetails by User id
	public List<PaymentCardDetails> findPaymentCardByUserId(int userIid) {
		return paymentCardDao.findPaymentCardByUserId(userIid);
	}
		
// Update PaymentCardDetails
	public PaymentCardDetails updatePaymentCardDetails(PaymentCardDetails paymentCardDetails) {

		return paymentCardDao.save(paymentCardDetails);
	}

//Delete PaymentCardDetails By id
	public int deletePaymentCardDetails(int id) {
		if (paymentCardDao.existsById(id)) {
			paymentCardDao.deleteById(id);
			return 1;
		} else
			return 0;
	}

	public PaymentCardDetails addpaymentCardDetails(@Valid PaymentCardDetails paymentCardDetails) {
		return paymentCardDao.save(paymentCardDetails);
	}
}
