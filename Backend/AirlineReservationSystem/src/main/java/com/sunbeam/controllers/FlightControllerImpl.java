package com.sunbeam.controllers;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.daos.FlightScheduleDtoProjection;
import com.sunbeam.dtos.FlightDto;
import com.sunbeam.dtos.FlightScheduleDto;
import com.sunbeam.dtos.Response;
import com.sunbeam.entities.Flight;
import com.sunbeam.services.FlightServiceImpl;

@CrossOrigin(origins = "*")
@RequestMapping("/flights")
@RestController
public class FlightControllerImpl {

@Autowired
private FlightServiceImpl flightService;

//Find all
@GetMapping("/")
public ResponseEntity<?> findAll(){
	try {
		List<Flight> flights=flightService.findAllFlight();
		if(flights!=null)
			return Response.success(flights);
		else
			return Response.error("No flight details available right now !!!");
	} catch (Exception e) {
		return Response.error(e.getMessage());
	}
}

// Add flight
@PostMapping("/add")
public ResponseEntity<?> addFlight (@RequestBody Flight flight)
{
	try {
		Flight f= flightService.addFlight(flight);
		if(f==null)
			return Response.error("Failed to add flight");
	return Response.success(f);	
	} catch (Exception e) {
		return Response.error(e.getMessage());
	}
}

//Get Flight by ID
	@GetMapping("/{id}")
	public ResponseEntity<?> FindFlightById( @PathVariable("id") int id) {
		try {
			Flight result = flightService.findFlightById(id);
			if (result == null)
				return Response.error("Flight by this id does not exists");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
//search flight	
	@GetMapping("/searchflights/{source}/{destination}/{departureDate}")
	public ResponseEntity<?> searchFlight(@PathVariable(value = "source") String sourceCity,@PathVariable(value = "destination") String destinationCity, @PathVariable(value = "departureDate") Date departureDate){
		try {
			System.out.println(sourceCity+" "+destinationCity+" "+departureDate);
		List<FlightScheduleDtoProjection> list=flightService.searchFlight(destinationCity, sourceCity, departureDate);
		if(list==null)
			return Response.error("Flight Not available for this journey");
		else
			return Response.success(list);
		}catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

//Check Flight statust	
		@GetMapping("/checkStatus/{flightid}/{departureDate}")
		public ResponseEntity<?> searchFlight(@PathVariable(value = "flightid") int flightid, @PathVariable(value = "departureDate") Date departureDate){
			try {
				System.out.println(flightid+" "+departureDate);
			List<FlightScheduleDtoProjection> list=flightService.checkFlightStatus(flightid, departureDate);
			if(list==null)
				return Response.error("Flight Not available for this journey");
			else
				return Response.success(list);
			}catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}
			
	// Update Flight
		@PutMapping("/{id}")
		public ResponseEntity<?> updateFlight(@PathVariable("id") int id, @RequestBody Flight flight){
			try {
				Flight f=flightService.findFlightById(id);
				if(f==null)
					return Response.error("Flight by this id does not exists");
				else
					{
					//f.setAirline_name(flight.getAirline_name());
					//f.setBusiness_class_capacity(flight.getBusiness_class_capacity());
					f.setBusiness_class_fare(flight.getBusiness_class_fare());
					//f.setEconomy_class_capacity(flight.getEconomy_class_capacity());
					f.setEconomy_class_fare(flight.getEconomy_class_fare());
					flightService.updateFlight(f);
					return Response.success(f);
					}
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}
		
	// Delete Flight by Id
		@DeleteMapping("/{id}")
		public ResponseEntity<?> deleteFlight(@PathVariable("id") int id) {
			try {
				Flight result = flightService.findFlightById(id);
				if (result == null)
					return Response.error("Flight by this id does not exists");
				else
				{
					int x=flightService.deletePassenger(id);
					return Response.success(x+" Flight deleted");
				}
				
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}
}
