package com.sunbeam.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sunbeam.dtos.Response;
import com.sunbeam.entities.Package;
import com.sunbeam.services.PackageServiceImpl;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/packages")
public class PackageControllerImpl {

	@Autowired
	private PackageServiceImpl packageService;

// Get Packages by ID
	@GetMapping("/{id}")
	public ResponseEntity<?> FindPackagesById(@PathVariable("id") int id) {
		try {
			Package result = packageService.findpackageById(id);
			if (result == null)
				return Response.error("Packages by this id does not exists");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Find all Packages
	@GetMapping("/")
	public ResponseEntity<?> Findall() {
		try {
			List<Package> result = packageService.findAllPackages();
			if (result == null)
				return Response.error("No Packages exists");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Add Packages
	@PostMapping("/add")
	public ResponseEntity<?> addPackages(@Valid @RequestBody Package Package) {
		try {
			Package p = packageService.addPackage(Package);
			if (p == null)
				return Response.error("Failed to add Packages");
			return Response.success(p);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Update Packages
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePackages(@PathVariable("id") int id, @Valid @RequestBody Package Packages) {
		try {
			Package p = packageService.findpackageById(id);
			if (p == null)
				return Response.error("Package by this id does not exists");
			else {
				p.setSeat_type(Packages.getSeat_type());
				p.setFood(Packages.getFood());
				p.setBeverages(Packages.getBeverages());
				p.setPackage_fare(Packages.getPackage_fare());
				p.setBaggage(Packages.getBaggage());
				Package p1 = packageService.updatePackage(p);
				return Response.success(p1);
			}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Delete Packages by Id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePackages(@PathVariable("id") int id) {
		try {
			Package result = packageService.findpackageById(id);
			if (result == null)
				return Response.error("Package by this id does not exists");
			else {
				int x = packageService.deletePackageById(id);
				return Response.success(x + " Package deleted");
			}

		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}
