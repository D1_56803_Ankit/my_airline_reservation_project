package com.sunbeam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import com.sunbeam.dtos.Response;
import com.sunbeam.dtos.ScheduleDto;
import com.sunbeam.entities.Flight;
import com.sunbeam.entities.Schedule;
import com.sunbeam.services.ScheduleServiceImpl;

@CrossOrigin(origins = "*")
@RequestMapping("/schedules")
@RestController
public class ScheduleControllerImpl {

	@Autowired
	private ScheduleServiceImpl scheduleService;
	
	//Add a schedule
	@PostMapping("/")
	public ResponseEntity<?> addSchedule(@RequestBody ScheduleDto schedule)
	{
		try {
			Schedule s=scheduleService.addSchedule(schedule);
			if(s!=null)
			return Response.success(s);
			else
				return Response.error("Failed to add schedule");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
		
	}
	
	//Find all schedules
	@GetMapping("/")
	public ResponseEntity<?> findAll()
	{
		try {
			List<Schedule> list=scheduleService.findAll();
			if(list!=null)
				return Response.success(list);
			return Response.error("No schedules available currently");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
	//Get Schedule by ID
		@GetMapping("/{id}")
		public ResponseEntity<?> FindscheduleById( @PathVariable("id") int id) {
			try {
				Object result = scheduleService.findSchedule(id);
				if (result == null)
					return Response.error("schedule by this id does not exists");
				return Response.success(result);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}
		
		// Update Schedule
			@PutMapping("/{id}")
			public ResponseEntity<?> updateSchedule(@PathVariable("id") int id, @RequestBody Schedule schedule){
				try {
					Object f=scheduleService.findSchedule(id);
					if(f==null)
						return Response.error("Schedule by this id does not exists");
					else
						{
						Schedule s=(Schedule)f;
						s.setFlight_status(schedule.getFlight_status());
						scheduleService.updateSchedule(s);
						return Response.success(f);
						}
				} catch (Exception e) {
					return Response.error(e.getMessage());
				}
			}
			
		// Delete Schedule by Id
			@DeleteMapping("/{id}")
			public ResponseEntity<?> deleteFlight(@PathVariable("id") int id) {
				try {
					Object f= scheduleService.findSchedule(id);
					if (f == null)
						return Response.error("Schedule by this id does not exists");
					else
					{
						Schedule s=(Schedule)f;
						int x=scheduleService.deleteSchedule(id);
						return Response.success(x+" Schedule deleted");
					}
					
				} catch (Exception e) {
					return Response.error(e.getMessage());
				}
			}
}
