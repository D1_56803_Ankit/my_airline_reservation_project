package com.sunbeam.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.Response;
import com.sunbeam.entities.Passenger;
import com.sunbeam.services.PassengerServiceImpl;

@CrossOrigin(origins = "*")
@RequestMapping("/passengers")
@RestController
public class PassengerControllerImpl {

	@Autowired
	private PassengerServiceImpl passengerService;

// Get Passenger by ID
	@GetMapping("/{id}")
	public ResponseEntity<?> FindPassengerById( @PathVariable("id") int id) {
		try {
			Passenger result = passengerService.findPassengerById(id);
			if (result == null)
				return Response.error("Passenger by this id does not exists");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Find all Passengers
	@GetMapping("/")
	public ResponseEntity<?> Findall() {
		try {
			List<Passenger> result = passengerService.findall();
			if (result == null)
				return Response.error("No Passengers exists");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Add Passenger
	@PostMapping("/add")
	public ResponseEntity<?> addPassenger (@Valid @RequestBody Passenger passenger)
	{
		System.out.println(passenger);
		try {
			Passenger p= passengerService.addPassenger(passenger);
			if(p==null)
				return Response.error("Failed to add passenger");
		return Response.success(p);	
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Add passenger while booking 	
	@PutMapping("/addPassenger")
	public ResponseEntity<?> addOrUpdatePassenger(@Valid @RequestBody Passenger passenger) {
		try{
			System.out.println(passenger);
		
		Passenger p = passengerService.saveOrUpdate(passenger);
		if(p!=null)
			return Response.success(p);
			else
				return Response.error("Error while adding passenger");
		}catch (Exception e) {
			return Response.error(e.getMessage());
		}
		}
	
// Update passenger
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePassenger(@PathVariable("id") int id, @Valid @RequestBody Passenger passenger){
		try {
			Passenger p=passengerService.findPassengerById(id);
			if(p==null)
				return Response.error("Passenger by this id does not exists");
			else
				{
				p.setAddress(passenger.getAddress());
				p.setCovid_vaccinated(passenger.getCovid_vaccinated());
				p.setEmail(passenger.getEmail());
				p.setMobile_no(passenger.getMobile_no());
				Passenger p1=passengerService.updatePassenger(p);
				return Response.success(p1);
				}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Delete Passenger by Id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePassenger(@PathVariable("id") int id) {
		try {
			Passenger result = passengerService.findPassengerById(id);
			if (result == null)
				return Response.error("Passenger by this id does not exists");
			else
			{
				int x=passengerService.deletePassenger(id);
				return Response.success(x+" Passenger deleted");
			}
			
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}
