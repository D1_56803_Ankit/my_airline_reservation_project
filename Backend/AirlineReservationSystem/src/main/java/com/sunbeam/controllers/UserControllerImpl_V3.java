//with the help of special class--Response 

package com.sunbeam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.Response;
import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;

@RequestMapping("/users/v3")
@RestController // @Controller + @ResponseBody.
public class UserControllerImpl_V3 {

	@Autowired
	private UserServiceImpl userService;

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") int userid) {
		try {
			User result = userService.findUserById(userid);
			if (result == null)
				return Response.error("User not found");
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}

	}

	@GetMapping("")
	public ResponseEntity<?> findAllUser() {
		try {
			List<User> result = userService.findAllUsers();
			if (result != null)
				return Response.success(result);
			return Response.error("No users found");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}

	}

	@PostMapping("/add")
	public ResponseEntity<?> addUser(@RequestBody User user) {
		try {
			System.out.println("In controller--" + user);
			int count = userService.addUser(user);
			if (count != 0)
				return Response.success(count + "user added");
			return Response.error("Failed to add user");
		} catch (Exception e) {
			return ResponseEntity.ok(e.getMessage());
		}

	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateUserById(@PathVariable("id") int id, @RequestBody User user) {

		try {
			System.out.println("In controller postman data: " + user);
			User usr = userService.findUserById(id);
			if (usr != null) {
				System.out.println("In controller before" + usr);
				usr.setFirst_name(user.getFirst_name());
				usr.setLast_name(user.getLast_name());
				usr.setPassword(user.getPassword());
				System.out.println("In controller after" + usr);
				User result = userService.updateUser(usr);
				return Response.success(result);
			}
			return Response.error("User not found by this ID");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUserById(@PathVariable("id") int userId) {

		try {
			int x = userService.deleteUserById(userId);
			if (x == 1)
				return Response.success(x + "user deleted");
			return Response.error("User not found by this ID");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

}
