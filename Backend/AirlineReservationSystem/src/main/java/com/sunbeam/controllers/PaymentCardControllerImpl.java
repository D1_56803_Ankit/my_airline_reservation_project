
package com.sunbeam.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sunbeam.dtos.Response;
import com.sunbeam.entities.PaymentCardDetails;
import com.sunbeam.services.PaymentCardServiceImpl;

@CrossOrigin(origins = "*")
@RequestMapping("/paymentcards")
@RestController
public class PaymentCardControllerImpl {

	@Autowired
	private PaymentCardServiceImpl paymentCardService;

// Get Payment Card Details by ID
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") int PaymentCardDetailsid) {
		try {
			PaymentCardDetails result = paymentCardService.findPaymentCardDetailsById(PaymentCardDetailsid);
			if (result == null)
				return Response.error("PaymentCardDetails not found");
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Get Payment Card Details by User ID
	@GetMapping("/user/{userId}")
	public ResponseEntity<?> findPaymentCardByUserId(@PathVariable("userId") int userId) {
		try {
			System.out.println(userId);
			List<PaymentCardDetails> result = paymentCardService.findPaymentCardByUserId(userId);
			if (result == null)
				return Response.error("PaymentCardDetails not found");
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Get all Payment Card Details 
	@GetMapping("/all")
	public ResponseEntity<?> findAllPaymentCardDetails() {
		try {
			List<PaymentCardDetails> result = paymentCardService.findAllPaymentCardDetails();
			if (result != null)
				return Response.success(result);
			return Response.error("No PaymentCardDetailss found");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

	// Add Payment Card Details
	@PostMapping("/add")
	public ResponseEntity<?> addPaymentCardDetails(@Valid @RequestBody PaymentCardDetails paymentCardDetails) {
		try {
			PaymentCardDetails card = paymentCardService.addpaymentCardDetails(paymentCardDetails);
			if (card == null)
				return Response.error("Failed to add payment Card Details");
			return Response.success(card);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Update Payment Card Details by id
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePaymentCardDetailsById(@PathVariable("id") int id,
			@RequestBody PaymentCardDetails paymentCardDetails) {
		try {
			PaymentCardDetails card = paymentCardService.findPaymentCardDetailsById(id);
			if (card == null)
				return Response.error("Payment Card by this id does not exists");
			else {
				card.setCard_no(paymentCardDetails.getCard_no());
				card.setName_on_card(paymentCardDetails.getName_on_card());
				card.setValidity_date(paymentCardDetails.getValidity_date());
				card.setCvv(paymentCardDetails.getCvv());
				paymentCardService.updatePaymentCardDetails(card);
				return Response.success(card);
			}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Delete Payment Card Details by id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePaymentCardDetailsById(@PathVariable("id") int id) {
		try {
			int x = paymentCardService.deletePaymentCardDetails(id);
			if (x == 1)
				return Response.success(x + " PaymentCardDetails deleted");
			return Response.error("Payment Card not found by this ID");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}
