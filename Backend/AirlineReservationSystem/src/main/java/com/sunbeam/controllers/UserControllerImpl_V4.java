
package com.sunbeam.controllers;

import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.Credentials;
import com.sunbeam.dtos.Response;
import com.sunbeam.entities.EmailRequest;
import com.sunbeam.entities.User;
import com.sunbeam.services.EmailService;
import com.sunbeam.services.UserServiceImpl2;

@CrossOrigin(origins = "*")
@RequestMapping("/users/v4")
@RestController
public class UserControllerImpl_V4 {

	@Autowired
	private UserServiceImpl2 userService;
	@Autowired
	private EmailService emailService;
	Random random = new Random();
	
//For user sign Up 
	@PostMapping("/signup")
	public ResponseEntity<?> SignUp(@Valid @RequestBody User user) {
		System.out.println("In controller--" + user);
		int count = userService.saveUser(user);
		if (count != 0)
			return Response.success(count + " User Added");
		return Response.error("User addition failed");
	}

//For User Sign In
	@PostMapping("/signin")
	public ResponseEntity<?> signIn(@Valid @RequestBody Credentials cred) {
		User user = userService.findUserByEmailAndPassword(cred);
		if (user == null)
			return Response.error("User not found");
		return Response.success(user);
	}

// Get User by ID
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") int userid) {
		try {
			User result = userService.findUserById(userid);
			if (result == null)
				return Response.error("User not found");
			return Response.success(result); // http status = 200, body = result
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Get all User 
	@GetMapping("")
	public ResponseEntity<?> findAllUser() {
		try {
			List<User> result = userService.findAllUsers();
			if (result != null)
				return Response.success(result);
			return Response.error("No users found");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Update user by id (Name changes)
	@PutMapping("/{id}")
	public ResponseEntity<?> updateUserById(@PathVariable("id") int id, @RequestBody User user) {
		try {
			User result = userService.updateUser(id, user);
			if (result != null)
				return Response.success(result);
			else
				return Response.error("User not found by this ID");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Change Password
	@PatchMapping("/resetpass/{id}/{currentPass}/{newPass}")
	public ResponseEntity<?> changePassword(@PathVariable("id") int id, @PathVariable("currentPass") String currentPass, @PathVariable("newPass") String newPass) {
		try {
			System.out.println(id+" "+currentPass+" "+newPass);
			Object result = userService.changePassword(id, currentPass, newPass);
			if (result == null)
				return Response.error("User not found by this ID");
			else if (result instanceof User) {
				User user = (User) result;
				return Response.success(user);
			}else
				return Response.error(result);	
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

//Email sender for Forgot password
	@PostMapping("/sendmail")
	public ResponseEntity<?> forgotPassword(@RequestBody EmailRequest email) {
		try {
			User currentUser=userService.findUserByEmail(email.getTo());
			if(currentUser!=null)
			{
			System.out.println(email);
			int otp = random.nextInt(999999);
			String message = email.getMessage()+otp;
			System.out.println("OTP :"+otp);
			boolean b=emailService.sendEmail(email.getSubject(), message, email.getTo());
			if (b == true)
				return Response.success(otp);
			else
				return Response.error("Email sent failed");
			}
			else
				return Response.error("User with this Email does not exists..!!");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
	
// Change forgotten Password
		@PatchMapping("/changepass/{emailto}/{newPass}")
		public ResponseEntity<?> changePassword(@PathVariable("emailto") String emailto, @PathVariable("newPass") String newPass) {
			try {
				User currentUser=userService.findUserByEmail(emailto);
				if(currentUser!=null)
				{
					System.out.println(emailto+" "+newPass+" "+currentUser);
				Object result = userService.changeForgotPassword(currentUser, newPass);
				return Response.success(result);
				}
				else return Response.error("User not found by this email");
						
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}
		
// Delete user by id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUserById(@PathVariable("id") int userId) {
		try {
			int x = userService.deleteUserById(userId);
			if (x == 1)
				return Response.success(x + " user deleted");
			return Response.error("User not found by this ID");
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}
