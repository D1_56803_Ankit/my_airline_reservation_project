//Response with http status and body

package com.sunbeam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;

@RequestMapping("/users/v2")
@RestController // @Controller + @ResponseBody. 
public class UserControllerImpl_V2 {

@Autowired
private UserServiceImpl userService;

@GetMapping("/{id}")
public ResponseEntity<?> findById (@PathVariable("id") int userid)
{
	User result=userService.findUserById(userid);
	if(result==null)
		return ResponseEntity.notFound().build(); // http status = 404
	return ResponseEntity.ok(result); // http status = 200, body = result
}

@GetMapping("")
public ResponseEntity<?> findAllUser() {
	List<User> result= userService.findAllUsers();
	if(result!=null)
		return ResponseEntity.ok(result);
	return ResponseEntity.notFound().build(); 
}

@PostMapping("/add")
public ResponseEntity<?> addUser(@RequestBody User user) {
	System.out.println("In controller--"+user);
	int count=userService.addUser(user);
	if(count!=0)
	return ResponseEntity.ok(count+"user added");
	return ResponseEntity.notFound().build();
}

@PutMapping("/{id}")
public ResponseEntity<?> updateUserById(@PathVariable("id") int id, @RequestBody User user) {
	System.out.println("In controller postman data: "+user);
	User usr=userService.findUserById(id);
	if(usr!=null)
	{
	usr.setFirst_name(user.getFirst_name());
	usr.setLast_name(user.getLast_name());
	usr.setPassword(user.getPassword());
	User result= userService.updateUser(usr);
	return ResponseEntity.ok(result);
	}
	return ResponseEntity.notFound().build();
	
}

@DeleteMapping("/{id}")
public ResponseEntity<?> deleteUserById(@PathVariable("id") int userId) {
	int x=userService.deleteUserById(userId);
	if(x==1)
		return ResponseEntity.ok(x+"user deleted");
	return ResponseEntity.notFound().build();
}

}
