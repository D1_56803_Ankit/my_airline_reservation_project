package com.sunbeam.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.sunbeam.daos.BookingDetailsProjection;
import com.sunbeam.dtos.BookingDto;
import com.sunbeam.dtos.Response;
import com.sunbeam.entities.Booking;
import com.sunbeam.entities.Offers;
import com.sunbeam.entities.User;
import com.sunbeam.services.BookingServiceImpl;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/bookings")
public class BookingController {
	
	@Autowired
	private BookingServiceImpl bookingService;
	
// Find all Bookings	
	@GetMapping("/") 
	public ResponseEntity<?> findAllBookings(){
		try {
			List<Booking> list=bookingService.findAllBookigs();
			if(list.isEmpty())
				return Response.error("No bookings available..!!");
			else
				return Response.success(list);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Get Booking by ID
		@GetMapping("/{id}")
		public ResponseEntity<?> FindBookingById(@PathVariable("id") int id) {
			try {
				Booking result = bookingService.findBookingById(id);
				if (result == null)
					return Response.error("Booking by this id does not exists");
				return Response.success(result);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}	

// Add Bookings
		@PostMapping("/add")
		public ResponseEntity<?> addBooking( @RequestBody BookingDto bookingDto) {
			try {
				System.out.println(bookingDto);
				Booking  b = bookingService.addBooking(bookingDto);
				if (b == null)
					return Response.error("Failed to add Booking");
				return Response.success(b);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}		

		
//Get all booking details for tickit generation	
		@GetMapping("/all")
		public ResponseEntity<?> getAll(){
			try {
				List<BookingDetailsProjection> list=bookingService.getAllBookingDetails();
				if(list.isEmpty())
					return Response.error("No bookings available..!!");
				else
					return Response.success(list);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
			
		}

// Get booking details by id for tickit generation
		@GetMapping("/booking/{bookingId}")
		public ResponseEntity<?> getBookingDetailsById(@PathVariable("bookingId") int bookingId) {
			try {
				BookingDetailsProjection result = bookingService.getBookingDetailsById(bookingId);
				if (result == null)
					return Response.error("Booking by this id does not exists");
				return Response.success(result);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
					
		}
		
// Get booking details by id for tickit generation
		@GetMapping("/mybookings/{userId}")
		public ResponseEntity<?> findByBooking(@PathVariable("userId") int userId) {
			try {
				List<BookingDetailsProjection> result = bookingService.findMyBooking(userId);
				if (result.isEmpty())
					return Response.error("Booking by this id does not exists");
					return Response.success(result);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
							
		}

//Check In		
		@PutMapping("/checkin/{bookingId}/{firstName}")
		public ResponseEntity<?> checkIn(@PathVariable("bookingId") int bookingId, @PathVariable("firstName") String firstName){
			
			Object result= bookingService.checkIn(bookingId, firstName);
			
			if(result==null)
				return Response.error("Booking with given details not found");
			else if(result=="Confirmed")
				return Response.error("Already Checked In");
			else if(result=="Cancelled")
				return Response.error("This Ticket is Cancelled");
			return Response.success(result);
			
		}

// My Trip		
		@GetMapping("/mytrip/{bookingId}/{firstName}")
		public ResponseEntity<?> myTrip(@PathVariable("bookingId") int bookingId, @PathVariable("firstName") String firstName){
			
			Booking result= bookingService.myTrip(bookingId, firstName);
			
			if(result==null)
				return Response.error("Booking with given details not found");
			return Response.success(result);
			
		}
		
		
// Cancel booking 
		@PatchMapping("/cancel/{bookingId}")
		public ResponseEntity<?> cancelBooking(@PathVariable("bookingId") int bookingId) {
			try {
				System.out.println(bookingId);
				Object result = bookingService.cancelBooking(bookingId);
				System.out.println(result);
				if (result == null)
					return Response.error("Booking not found by this ID");
				else 
					if(result.equals("Tickit is already cancelled"))
					return Response.error("Tickit is already cancelled");
				else 
					return Response.success(result);
			} catch (Exception e) {
				return Response.error(e.getMessage());
			}
		}	
}
