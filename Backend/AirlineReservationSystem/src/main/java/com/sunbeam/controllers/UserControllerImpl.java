
package com.sunbeam.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;

@RestController
public class UserControllerImpl {

@Autowired
private UserServiceImpl userService;

@GetMapping("/users/{id}")
public @ResponseBody Object findById (@PathVariable("id") int userid)
{
	User result=userService.findUserById(userid);
	if(result==null)
		return "User not found";
	return result;
}

@GetMapping("/users")
public List<User> findAllUser() {
	return userService.findAllUsers();
}

@PostMapping("/users/add")
public String addUser(@RequestBody User user) {
	System.out.println("In controller--"+user);
	int count=userService.addUser(user);
	if(count!=0)
	return count+" User Added";
	return "User addition failed";
}

@PutMapping("/users/{id}")
public Object updateUserById(@PathVariable("id") int id, @RequestBody User user) {
	System.out.println("In controller postman data: "+user);
	User usr=userService.findUserById(id);
	if(usr!=null)
	{
	System.out.println("In controller before"+usr);
	usr.setFirst_name(user.getFirst_name());
	usr.setLast_name(user.getLast_name());
	usr.setPassword(user.getPassword());
	System.out.println("In controller after"+usr);
		return userService.updateUser(usr);
	}
	return "User not found by this ID";
	
}

@DeleteMapping("/users/{id}")
public String deleteUserById(@PathVariable("id") int userId) {
	int x=userService.deleteUserById(userId);
	if(x==1)
		return "User deleted "+x;
	return "User with this id does not exist";
}

}
